package com.hzc.easyexcel.config;

import java.lang.annotation.*;

/**
 * @Classname IsNullConvertZero
 * @Description TODO
 * @Date 2021/9/30 17:04
 * @Created by hzc
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface IsNullConvertZero {
}
