package com.hzc.easyexcel.config;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @Classname Message
 * @Description TODO
 * @Date 2021/9/14 23:54
 * @Created by hzc
 */
@Data
public class Message {
    @ApiModelProperty(value = "返回码")
    private Integer code;

    @ApiModelProperty(value = "返回消息")
    private String meg;

}
