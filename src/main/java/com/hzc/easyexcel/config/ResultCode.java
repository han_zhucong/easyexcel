package com.hzc.easyexcel.config;

/**
 * @Classname ResultCode
 * @Description TODO
 * @Date 2021/2/18 17:14
 * @Created by hzc
 */
public interface ResultCode {
    public static Integer SUCCESS = 20000;
    public static Integer ERROR = 20001;

}
