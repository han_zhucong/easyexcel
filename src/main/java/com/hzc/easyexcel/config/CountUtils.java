package com.hzc.easyexcel.config;

import com.hzc.easyexcel.entity.FirstData;
import com.hzc.easyexcel.entity.SecondData;
import com.hzc.easyexcel.entity.Structure;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @Classname CountUtils
 * @Description TODO
 * @Date 2021/9/30 15:14
 * @Created by hzc
 */

public class CountUtils {


    //OK
    public BigDecimal countFormerArea(FirstData firstData) {
        //BigDecimal protectionArea, BigDecimal sevenArea, BigDecimal fiveArea,
        // BigDecimal twoArea, BigDecimal otherArea, BigDecimal resetArea, BigDecimal brick, BigDecimal wood, BigDecimal fish
        BigDecimal num1 = new BigDecimal("0.7");
        BigDecimal sevenArea = firstData.getSevenArea();
        BigDecimal count2 = sevenArea.multiply(num1);

        BigDecimal num2 = new BigDecimal("0.5");
        BigDecimal fiveArea = firstData.getFiveArea();
        BigDecimal count3 = fiveArea.multiply(num2);

        BigDecimal num3 = new BigDecimal("0.2");
        BigDecimal twoArea = firstData.getTwoArea();
        BigDecimal count4 = twoArea.multiply(num3);

        BigDecimal getProtectionArea = firstData.getProtectionArea();
        BigDecimal otherArea = firstData.getOtherArea();

        BigDecimal formerArea = getProtectionArea.add(count2).add(count3).add(count4).add(otherArea);

        return formerArea;
    }

    //计算公摊面积  Ok
    public SecondData countSharedArea(SecondData secondData){
        BigDecimal formerArea = secondData.getFormerArea();
        BigDecimal num1 = new BigDecimal("0.1");
        BigDecimal compare = new BigDecimal("10");
        //计算公摊面积
        BigDecimal sharedArea = formerArea.multiply(num1);
        //==1 : 大于 ==-1：小于  ==0：等于
        if (sharedArea.compareTo(compare) == 1){
            secondData.setSharedArea(compare);
        }else {
            secondData.setSharedArea(sharedArea);
        }

        return secondData;
    }

    //获取确权面积  OK
    public SecondData countAuthorizedArea(SecondData secondData){
        BigDecimal formerArea = secondData.getFormerArea();
        BigDecimal sharedArea = secondData.getSharedArea();
        BigDecimal authorizedArea = formerArea.add(sharedArea);
        secondData.setAuthorizedArea(authorizedArea);
        return secondData;
    }

    // TODO: 2021/10/1  OK
    //判断签约期
    public SecondData judgeSignTime(SecondData secondData){
        try {
            String signTime = null;
            String dataSignTime = secondData.getSignTime();
            BigDecimal num1 = new BigDecimal(0.2);
            BigDecimal num2 = new BigDecimal(0.1);
            if (dataSignTime != null){
                signTime = dataSignTime;
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date date1 = sdf.parse(signTime);
                Date parse1 = sdf.parse("2013-11-21");
                Date parse2 = sdf.parse("2013-12-31");
                Date parse3 = sdf.parse("2014-1-1");
                Date parse4 = sdf.parse("2014-1-15");
                System.out.println(date1.compareTo(parse2));
                if (date1.compareTo(parse1) > 0 && date1.compareTo(parse2) < 0 ){
                    secondData.setSignTimeBySign(num1);

                }else if (date1.compareTo(parse3) > 0 && date1.compareTo(parse4) < 0 ){
                    secondData.setSignTimeBySign(num2);

                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return secondData;
    }

    //判断房屋结构并计算旧房补偿费(如果多个结构，一个人的多个结构对应多个Structure对象相加)
    public SecondData setSecondOldHouseMoney(SecondData secondData,Structure structure){
        BigDecimal allPrice = new BigDecimal(0);

        List<BigDecimal> num = new ArrayList<>();
        List<BigDecimal> rate = new ArrayList<>();
        List<BigDecimal> area = new ArrayList<>();
        rate.add(structure.getRate1());
        rate.add(structure.getRate2());
        rate.add(structure.getRate3());
        area.add(structure.getArea1());
        area.add(structure.getArea2());
        area.add(structure.getArea3());

        String structure1 = secondData.getStructure();

        if(structure1!=null){
            String[] structures = structure1.split("、");
            for (String name:structures){
                num.add(structure.putPrice(name));
            }
            System.out.println(num.size());
            for (int i=0; i<num.size();i++){
                BigDecimal num1 = num.get(i);
                BigDecimal num2 = rate.get(i);
                BigDecimal num3 = area.get(i);
                allPrice = allPrice.add(num1.multiply(num2).multiply(num3));

            }

        }
        secondData.setSecondOldHouseMoney(allPrice);

        return secondData;
    }

    //旧房补偿费的审计调整
    public SecondData SetFirstAdjustment(SecondData secondData){
        //审定数
        BigDecimal secondOldHouseMoney = secondData.getSecondOldHouseMoney();
        //送审数
        BigDecimal oldHouseMoney = secondData.getOldHouseMoney();
        secondData.setFirstAdjustment(secondOldHouseMoney.subtract(oldHouseMoney));
        return secondData;
    }

    //区位补偿费的审计调整  OK
    public SecondData SetThirdAdjustment(SecondData secondData){
        //审定数
        BigDecimal locationCompensationYes = secondData.getLocationCompensationYes();
        //送审数
        BigDecimal locationCompensationNo = secondData.getLocationCompensationNo();
        secondData.setThirdAdjustment(locationCompensationYes.subtract(locationCompensationNo));
        return secondData;
    }

    // TODO: 2021/10/8
    //区位补偿费的审定数
    public SecondData SetLocationCompensationYes(SecondData secondData){
        //"总确权面积","(㎡)","审定数"
        BigDecimal authorizedArea = secondData.getAuthorizedArea();
        //"审计单价"
        BigDecimal unitPrice = secondData.getUnitPrice();

        secondData.setLocationCompensationYes(unitPrice.multiply(authorizedArea));
        return secondData;
    }

    //按期搬迁奖励审计调整 OK
    public SecondData SetFourthAdjustment(SecondData secondData){
        BigDecimal rewardYes = secondData.getRelocationRewardYes();
        BigDecimal rewardNo = secondData.getRelocationRewardNo();

        secondData.setFourthAdjustment(rewardYes.subtract(rewardNo));

        return secondData;
    }

    // TODO: 2021/10/8
    //24.2按期搬迁奖励的审定数
    public SecondData SetRelocationRewardYes(SecondData secondData){
        List<Structure> structureNew = secondData.getStructureNew();
        BigDecimal formerArea = secondData.getFormerArea();
        //获取比例系数
        BigDecimal signTimeBySign = secondData.getSignTimeBySign();

        BigDecimal relocationRewardYes = null;
        for (Structure structure : structureNew) {
            BigDecimal price = structure.getPrice();
            relocationRewardYes = relocationRewardYes.add(formerArea.multiply(price).multiply(signTimeBySign));
        }
        secondData.setRelocationAllowanceYes(relocationRewardYes);
        return secondData;
    }

    //货币补偿面积的审计调整  OK
    public SecondData SetFifthAdjustment(SecondData secondData){
        //审定数
        BigDecimal monetaryCompensationYes = secondData.getMonetaryCompensationYes();
        //未审数
        BigDecimal monetaryCompensationNo = secondData.getMonetaryCompensationNo();
        secondData.setFifthAdjustment(monetaryCompensationYes.subtract(monetaryCompensationNo));

        return secondData;
    }

    //25.4货币补偿面积的审定数 area:货币补偿面积  手动输入
    public SecondData SetMonetaryCompensationYes(BigDecimal area,SecondData secondData){
        BigDecimal num = new BigDecimal(1839);
        BigDecimal compensationArea = area.multiply(num);
        secondData.setMonetaryCompensationYes(compensationArea);
        return secondData;
    }

    //26.3安家补贴的审定数
    public SecondData SetRelocationAllowanceYes(SecondData secondData){
        BigDecimal num = new BigDecimal(50);
        //原房确权面积
        BigDecimal formerArea = secondData.getFormerArea();

        BigDecimal relocationAllowanceYes = formerArea.multiply(num);
        return secondData;
    }

    //25.4货币补偿面积的审定数
    public SecondData SetCompensationArea(SecondData secondData){
        BigDecimal num = new BigDecimal(1839);
        //货币补偿面积
        BigDecimal compensationArea = secondData.getCompensationArea();

        secondData.setCompensationArea(compensationArea.multiply(num));
        return secondData;
    }

    //2004年至2006年搬迁奖励的审计调整  OK
    public SecondData SetSeventhAdjustment(SecondData secondData){
        //审定数
        BigDecimal reviewedYes = secondData.getReviewedYes();
        //未审数
        BigDecimal reviewedNo = secondData.getReviewedNo();
        secondData.setSeventhAdjustment(reviewedYes.subtract(reviewedNo));

        return secondData;
    }

    //27.3 2004年至2006年搬迁奖励的审定数  OK
    public SecondData SetReviewedYes(SecondData secondData){
        //2004年-2006年 重置价面积
        BigDecimal resetArea = secondData.getResetArea();
        BigDecimal num = new BigDecimal(300);
        BigDecimal reviewedYes = resetArea.multiply(num);
        secondData.setReviewedYes(reviewedYes);
        return secondData;
    }

    //28.2 2006年后补贴的审计调整  OK
    public SecondData SetAudit(SecondData secondData){
        //审定数
        BigDecimal authorizedNum = secondData.getAuthorizedNum();
        //未审数
        BigDecimal unauditedSubsidiesNo = secondData.getUnauditedSubsidiesNo();

        secondData.setAudit(authorizedNum.subtract(unauditedSubsidiesNo));

        return secondData;
    }

    //28.3 2006年后补贴的审定数  OK
    public SecondData SetAuthorizedNum(SecondData secondData){
        BigDecimal brick = secondData.getBrick();
        BigDecimal wood = secondData.getWood();
        BigDecimal fish = secondData.getFish();
        BigDecimal num = new BigDecimal(150);

        BigDecimal authorizedNum = (brick.add(wood).add(fish)).multiply(num);

        secondData.setAuthorizedNum(authorizedNum);

        return secondData;
    }

    //29.2 简易补贴的审计调整  OK
    public SecondData SetAdjust(SecondData secondData){
        //审定数
        BigDecimal simpleSubTwo = secondData.getSimpleSubTwo();
        //未审数
        BigDecimal simpleSub = secondData.getSimpleSub();

        BigDecimal adjust = simpleSubTwo.subtract(simpleSub);

        secondData.setAdjust(adjust);

        return secondData;
    }

    //29.3 简易补贴的审定数  OK
    public SecondData SetSimpleSubTwo(SecondData secondData){
        BigDecimal simpleArea = secondData.getSimpleArea();
        BigDecimal num = new BigDecimal(70);

        BigDecimal simpleSubTwo = simpleArea.multiply(num);

        secondData.setSimpleSubTwo(simpleSubTwo);

        return secondData;
    }

    //逾期过渡费的审计调整
    public SecondData SetAdjustSeven(SecondData secondData){
        //审定数
        BigDecimal overdueOne = secondData.getOverdueOne();
        //未审数
        BigDecimal overdue = secondData.getOverdue();

        BigDecimal adjustSeven = overdueOne.subtract(overdue);

        secondData.setUnitPriceSeven(adjustSeven);

        return secondData;
    }

    // TODO: 2021/10/8 月份要手动输入
    //逾期过渡费金额的审定数
    public SecondData SetOverdueOne(SecondData secondData){

        //50.1 单价
        //50.2 月份

        return secondData;
    }

    //原房补偿补助费（元）合计（未审数）
    public SecondData SetAllPay(SecondData secondData){
        BigDecimal oldHouseMoney = secondData.getOldHouseMoney();
        BigDecimal compensationNo = secondData.getCompensationNo();
        BigDecimal locationCompensationNo = secondData.getLocationCompensationNo();
        BigDecimal relocationRewardNo = secondData.getRelocationRewardNo();
        BigDecimal monetaryCompensationNo = secondData.getMonetaryCompensationNo();
        BigDecimal relocationAllowanceNo = secondData.getRelocationAllowanceNo();
        BigDecimal reviewedNo = secondData.getReviewedNo();
        BigDecimal unauditedSubsidiesNo = secondData.getUnauditedSubsidiesNo();
        BigDecimal simpleSub = secondData.getSimpleSub();
        BigDecimal rentSubOne = secondData.getRentSubOne();
        BigDecimal holidayRentPayOne = secondData.getHolidayRentPayOne();
        BigDecimal moveReward = secondData.getMoveReward();
        BigDecimal holidayMovePay = secondData.getHolidayMovePay();
        BigDecimal otherPay = secondData.getOtherPay();
        BigDecimal tenPay = secondData.getTenPay();
        BigDecimal fifteenPay = secondData.getFifteenPay();
        BigDecimal fifteenTwoPay = secondData.getFifteenTwoPay();
        BigDecimal subsidyOne = secondData.getSubsidyOne();
        BigDecimal subsidyTwo = secondData.getSubsidyTwo();
        BigDecimal subsidyThree = secondData.getSubsidyThree();
        BigDecimal subsidy = secondData.getSubsidy();
        //BigDecimal storesArea = secondData.getStoresArea();
        BigDecimal rewardOne = secondData.getRewardOne();
        BigDecimal rewardTwo = secondData.getRewardTwo();
        BigDecimal relocationRewardOne = secondData.getRelocationRewardOne();
        BigDecimal relocationSubsidy = secondData.getRelocationSubsidy();
        BigDecimal slopeProtection = secondData.getSlopeProtection();
        BigDecimal meetingMinutes = secondData.getMeetingMinutes();
        BigDecimal compensation = secondData.getCompensation();
        BigDecimal accounts = secondData.getAccounts();
        BigDecimal overdue = secondData.getOverdue();

        BigDecimal allPay = oldHouseMoney.add(compensationNo).add(locationCompensationNo).add(relocationRewardNo)
                .add(monetaryCompensationNo).add(relocationAllowanceNo).add(reviewedNo)
                .add(unauditedSubsidiesNo).add(simpleSub).add(rentSubOne).add(holidayRentPayOne)
                .add(moveReward).add(holidayMovePay).add(otherPay)
                .add(tenPay).add(fifteenPay).add(fifteenTwoPay).add(subsidyOne)
                .add(subsidyTwo).add(subsidyThree).add(subsidy).add(rewardOne)
                .add(rewardTwo).add(relocationRewardOne).add(relocationSubsidy)
                .add(slopeProtection).add(meetingMinutes).add(compensation)
                .add(compensation).add(accounts).add(overdue);

        secondData.setAllPay(allPay);

        return secondData;
    }

    //原房补偿补助费（元）合计审定数
    public SecondData SetOriginalHouse(SecondData secondData){
        BigDecimal secondOldHouseMoney = secondData.getSecondOldHouseMoney();
        BigDecimal compensationYes = secondData.getCompensationYes();
        BigDecimal locationCompensationYes = secondData.getLocationCompensationYes();
        BigDecimal relocationRewardYes = secondData.getRelocationRewardYes();
        BigDecimal monetaryCompensationYes = secondData.getMonetaryCompensationYes();
        BigDecimal relocationAllowanceYes = secondData.getRelocationAllowanceYes();
        BigDecimal reviewedYes = secondData.getReviewedYes();
        BigDecimal authorizedNum = secondData.getAuthorizedNum();
        BigDecimal simpleSubTwo = secondData.getSimpleSubTwo();
        BigDecimal rentSubTwo = secondData.getRentSubTwo();
        BigDecimal holidayRentPayOne = secondData.getHolidayRentPayTwo();
        BigDecimal moveRewardTwo = secondData.getMoveRewardTwo();
        BigDecimal holidayMovePayTwo = secondData.getHolidayMovePayTwo();
        BigDecimal otherPayTwo = secondData.getOtherPayTwo();
        BigDecimal allowance = secondData.getAllowance();
        BigDecimal resettlementMonth = secondData.getResettlementMonth();
        //41.5
        BigDecimal rewardThree = secondData.getRewardThree();
        BigDecimal relocationReward = secondData.getRelocationReward();
        BigDecimal relocationSubsidyTwo = secondData.getRelocationSubsidyTwo();
        BigDecimal meetingMinutesTwo = secondData.getMeetingMinutesTwo();
        BigDecimal accountsYes = secondData.getAccountsYes();
        BigDecimal overdueOne = secondData.getOverdueOne();

        BigDecimal originalHouse = secondOldHouseMoney.add(compensationYes).add(locationCompensationYes).add(relocationRewardYes)
                .add(monetaryCompensationYes).add(relocationAllowanceYes).add(reviewedYes)
                .add(authorizedNum).add(simpleSubTwo).add(rentSubTwo).add(holidayRentPayOne)
                .add(moveRewardTwo).add(holidayMovePayTwo).add(otherPayTwo).add(allowance)
                .add(resettlementMonth).add(rewardThree).add(relocationReward).add(relocationSubsidyTwo)
                .add(meetingMinutesTwo).add(accountsYes).add(overdueOne);

        secondData.setOriginalHouse(originalHouse);

        return secondData;
    }



    //安置房面积
    public SecondData SetOnePay(SecondData secondData){
        BigDecimal onePay = new BigDecimal("80.00");
        //安置房面积,审定数
        BigDecimal area = secondData.getArea();
        //安置房内部隔墙一次性补偿及卫生洁具补偿（未审数）
        BigDecimal otherPay = secondData.getOtherPay();
        //审计单价
        secondData.setOnePay(onePay);
        //安置房内部隔墙一次性补偿及卫生洁具补偿（元）,审定数
        secondData.setOtherPayTwo(area.multiply(onePay));
        BigDecimal otherPayTwo = secondData.getOtherPayTwo();
        secondData.setAdjustTwo(otherPayTwo.subtract(otherPay));
        return secondData;
    }

    //37.2 37.5 37.6 房屋置换面积(不含公摊 搬迁补助费的审定数和审计调整 OK
    public SecondData SetReplaceArea(SecondData secondData){
        BigDecimal num = new BigDecimal(15);
        BigDecimal num1 = new BigDecimal(2);
        //原房确权面积
        BigDecimal formerArea = secondData.getFormerArea();
        //货币安置面积（不含公摊)
        BigDecimal settlementArea = secondData.getSettlementArea();
        //房屋置换原房确权面积（不含公摊)
        secondData.setReplaceArea(formerArea.subtract(settlementArea));
        BigDecimal replaceArea = secondData.getReplaceArea();

        //搬迁补助费（元）
        secondData.setAllowance(settlementArea.multiply(num).add(replaceArea.multiply(num).multiply(num1)));
        BigDecimal allowance = secondData.getAllowance();
        //搬迁补助费（元）未审数 10*1
        BigDecimal tenPay = secondData.getTenPay();
        //搬迁补助费（元）未审数 15*1
        BigDecimal fifteenPay = secondData.getFifteenPay();
        //搬迁补助费（元）未审数 15*2
        BigDecimal fifteenTwoPay = secondData.getFifteenTwoPay();
        //审计调整
        secondData.setAdjustThree(allowance.subtract(tenPay).subtract(fifteenPay).subtract(fifteenTwoPay));

        return secondData;
    }

    // TODO: 2021/10/4  (原来的公式错误)  OK
    //原房面积
    public SecondData SetOriginalArea(SecondData secondData){
        BigDecimal num = new BigDecimal(3);
        BigDecimal num1 = new BigDecimal(8);
        BigDecimal num2 = new BigDecimal(12);
        BigDecimal num3 = new BigDecimal(36);
        //原房确权面积
        BigDecimal formerArea = secondData.getFormerArea();
        //原房面积
        secondData.setOriginalArea(formerArea);
        BigDecimal originalArea = secondData.getOriginalArea();
        //审定单价
        secondData.setUnitPriceThree(num1);
        BigDecimal unitPriceThree = secondData.getUnitPriceThree();
        //期房安置面积
        BigDecimal placementArea = secondData.getPlacementArea();
        //期房安置面积2
        BigDecimal placementAreaTwo = secondData.getPlacementAreaTwo();
        //期房安置月份
        BigDecimal resettlemenMonth = secondData.getResettlemenMonth();
        //临时安置补助费（元）
        secondData.setResettlementMonth(originalArea.multiply(num).multiply(num1).add(placementArea.multiply(num2).multiply(num1)).add(placementAreaTwo.multiply(num1).multiply(num3)));
        BigDecimal resettlementMonth = secondData.getResettlementMonth();
        //临时安置补助费（元）未审数
        BigDecimal subsidyOne = secondData.getSubsidyOne();// 8元/月 （12个月）
        BigDecimal subsidyTwo = secondData.getSubsidyTwo();// 8元/月 （36个月）
        BigDecimal subsidyThree = secondData.getSubsidyThree();// 8元/月 （另3个月）
        secondData.setAdjustFour(resettlementMonth.subtract(subsidyOne).subtract(subsidyTwo).subtract(subsidyThree));
        return secondData;
    }

    // TODO: 2021/10/2 (少了一列)  OK
    //店面经营补助
    public SecondData SetOriginal(SecondData secondData){

        //店面经营补助（元） 未审数
        BigDecimal subsidy = secondData.getSubsidy();
        //店面面积
        BigDecimal storesArea = secondData.getStoresArea();
        //单价
        BigDecimal unitPriceFour = secondData.getUnitPriceFour();
        //店面经营补助（元） 审定数
        secondData.setOperationSubsidy(storesArea.multiply(unitPriceFour));
        BigDecimal operationSubsidy = secondData.getOperationSubsidy();
        //"店面经营补助","","审定调整"
        secondData.setAdjustFive(operationSubsidy.subtract(subsidy));


        return secondData;
    }

    //奖励（06年后） OK
    public SecondData SetRewardThree(SecondData secondData){
        BigDecimal num = new BigDecimal(130);
        BigDecimal num1 = new BigDecimal(150);
        //单价
        secondData.setUnitPriceFive(num);
        BigDecimal unitPriceFive = secondData.getUnitPriceFive();
        //"2006年后","150元面积（㎡）","砖混"
        BigDecimal brick = secondData.getBrick();
        //"2006年后","150元面积（㎡）","砖木"
        BigDecimal wood = secondData.getWood();
        //"2006年后","150元面积（㎡）","鱼鳞板(核对)"
        BigDecimal fish = secondData.getFish();

        //奖励（06年后） 审定数
        secondData.setRewardThree(brick.multiply(num1).add(wood.add(fish).multiply(unitPriceFive)));

        return secondData;
    }

    // TODO: 2021/10/3  公式与新附表二不匹配
    //决算增加补过渡费金额 审定数
    public SecondData SetAccountsYes(SecondData secondData){
        //面积
        BigDecimal areaOne = secondData.getAreaOne();
        //单价1
        BigDecimal priceOne = secondData.getPriceOne();
        //单价
        BigDecimal priceTwo = secondData.getPriceTwo();
        //月份1
        int monthOne = secondData.getMonthOne();
        BigDecimal num1 = new BigDecimal(monthOne);
        //月份1
        int monthTwo = secondData.getMonthTwo();
        BigDecimal num2 = new BigDecimal(monthTwo);

        BigDecimal num3 = new BigDecimal(1);

        //决算增加补过渡费金额 审定数
        secondData.setAccountsYes(areaOne.multiply(priceOne).multiply(num1).add(areaOne.multiply(priceTwo).multiply(num2)).subtract(num3));

        return secondData;
    }

}
