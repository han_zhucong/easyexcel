package com.hzc.easyexcel.config;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Classname PageConfig
 * @Description TODO
 * @Date 2021/9/26 14:05
 * @Created by hzc
 */
@Configuration
@MapperScan("com.hzc.easyexcel.mapper.SecondDataMapper")
public class PageConfig {
    /**
     * 分页插件
     * mybatis-plus-boot-starter版本不能是3.4以后的，3.4以后的PaginationInterceptor过时了
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        return new PaginationInterceptor();
    }
}
