package com.hzc.easyexcel.config;

import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.Objects;

/*
 *@auther:陈旭峰
 *@create time:2021/09/30 16:52
 *@description:
 *
 */
@Slf4j
public class IsNullConvertZeroUtil {

    public static Object checkIsNull(Object obj) {
        try {
            Class<?> clazz = obj.getClass();
            //获得私有的成员属性
            Field[] fields = clazz.getDeclaredFields();
            if(Objects.nonNull(fields) && fields.length>0){
                for(Field field : fields){
                    field.setAccessible(true);
                    //判断IsNullConvertZero注解是否存在
                    if(field.isAnnotationPresent(IsNullConvertZero.class)){
                        if(Objects.isNull(field.get(obj))){
                            field.set(obj, BigDecimal.ZERO);
                        }
                    }
                }
            }
        } catch (Exception e) {
            log.error("IsNullConvertZeroUtil出现异常：{}",e);
        }
        return obj;
    }

}
