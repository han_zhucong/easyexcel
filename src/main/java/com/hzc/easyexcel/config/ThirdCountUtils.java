package com.hzc.easyexcel.config;

import com.hzc.easyexcel.entity.SecondData;
import com.hzc.easyexcel.entity.ThirdData;

import java.math.BigDecimal;

/**
 * @Classname ThirdCountUtils
 * @Description TODO
 * @Date 2021/10/3 1:19
 * @Created by hzc
 */
public class ThirdCountUtils {
    //54.12 等面积金额测算（总确权面积） OK
    public ThirdData SetConfirmedArea(SecondData secondData, ThirdData thirdData){
        BigDecimal authorizedArea = secondData.getAuthorizedArea();
        thirdData.setConfirmedArea(authorizedArea);
        return thirdData;
    }

    //54.13 等面积金额测算（扣减货币补偿面积）OK
    public ThirdData SetCompensationArea(SecondData secondData,ThirdData thirdData){
        BigDecimal compensationArea = secondData.getCompensationArea();
        thirdData.setCompensationArea(compensationArea);
        return thirdData;
    }

    //54.14 等面积金额（测算测算置换等面积） OK
    public ThirdData SetReplacementArea(ThirdData thirdData){
        //等面积金额测算（总确权面积）
        BigDecimal confirmedArea = thirdData.getConfirmedArea();
        //等面积金额测算（扣减货币补偿面积）
        BigDecimal compensationArea = thirdData.getCompensationArea();
        BigDecimal replacementArea = confirmedArea.subtract(compensationArea);
        thirdData.setReplacementArea(replacementArea);
        return thirdData;
    }

    //54.15等面积金额（与等面积差异面积） OK
    public ThirdData SetUnauditedNumber(ThirdData thirdData){
        //54.14等面积金额（测算测算置换等面积）
        BigDecimal replacementArea = thirdData.getReplacementArea();
        //54.9 等面积（数量）未审数
        BigDecimal quantityOne = thirdData.getQuantityOne();
        BigDecimal unauditedNumber = replacementArea.subtract(quantityOne);
        thirdData.setUnauditedNumber(unauditedNumber);
        return thirdData;
    }

    //54.16 审定等面积 审定数量  OK
    public ThirdData SetApprovedQuantity(ThirdData thirdData){
        //54.9 等面积（数量）未审数
        BigDecimal quantityOne = thirdData.getQuantityOne();

        thirdData.setApprovedQuantity(quantityOne);
        return thirdData;
    }

    //54.17 审定等面积 单价 OK
    public ThirdData SetPriceThree(ThirdData thirdData){
        //54.10等面积（单价）未审数
        BigDecimal priceOne = thirdData.getPriceOne();

        thirdData.setPriceThree(priceOne);
        return thirdData;
    }

    //54.18 审定等面积金额 审计调整  OK
    public ThirdData SetAdjustmentOne(ThirdData thirdData){
        //54.19 审定等面积 审定金额
        BigDecimal amount = thirdData.getAmount();
        //54.11 等面积（金额）未审数
        BigDecimal moneyOne = thirdData.getMoneyOne();

        BigDecimal adjustmentOne = amount.subtract(moneyOne);
        thirdData.setAdjustmentOne(adjustmentOne);
        return thirdData;
    }

    //54.19 审定等面积 审定金额   OK
    public ThirdData SetAmount(ThirdData thirdData){
        //54.16 审定等面积 审定数量 ㎡
        BigDecimal approvedQuantity = thirdData.getApprovedQuantity();
        //54.17 审定等面积 单价
        BigDecimal priceThree = thirdData.getPriceThree();

        BigDecimal amount = approvedQuantity.multiply(priceThree);
        thirdData.setAmount(amount);
        return thirdData;
    }

    //55.03 审定上靠 审定上靠面积  OK
    public ThirdData SetQuantityTwo(ThirdData thirdData){
        //54.4安置面积
        BigDecimal resettlementArea = thirdData.getResettlementArea();
        //54.16 审定等面积
        BigDecimal approvedQuantity = thirdData.getApprovedQuantity();
        //56.3 审计结构差与标准户型差面积
        BigDecimal areaDifference = thirdData.getAreaDifference();
        //57.3 审定增房数量
        BigDecimal quantityFour = thirdData.getQuantityFour();

        BigDecimal quantityTwo = resettlementArea.subtract(approvedQuantity).subtract(areaDifference).subtract(quantityFour);
        thirdData.setQuantityTwo(quantityTwo);
        return thirdData;
    }

    //55.04 审定上靠 单价  OK
    public ThirdData SetPriceTwo(ThirdData thirdData){
        //55.01 上靠（单价）未审数
        BigDecimal price = thirdData.getPrice();

        thirdData.setPriceTwo(price);
        return thirdData;
    }

    //55.05 审计调整  OK
    public ThirdData SetAdjustmentTwo(ThirdData thirdData){
        //55.06 审顶上靠 金额
        BigDecimal moneyTwo = thirdData.getMoneyTwo();
        //55.02 上靠（金额）未审数
        BigDecimal money = thirdData.getMoney();

        BigDecimal adjustmentTwo = moneyTwo.subtract(money);
        thirdData.setAdjustmentTwo(adjustmentTwo);
        return thirdData;
    }

    //55.06 审顶上靠 金额  OK
    public ThirdData SetMoneyTwo(ThirdData thirdData){
        //55.03审定上靠面积
        BigDecimal quantityTwo = thirdData.getQuantityTwo();
        //55.04审定上靠单价
        BigDecimal priceTwo = thirdData.getPriceTwo();

        BigDecimal moneyTwo = quantityTwo.multiply(priceTwo);
        thirdData.setMoneyTwo(moneyTwo);
        return thirdData;
    }

    //56.3 审定结构差 与标准户型差面积  OK
    public ThirdData SetAreaDifference(ThirdData thirdData){

        BigDecimal num = new BigDecimal(0.9);
        BigDecimal num1 = new BigDecimal(1.1);
        //54.4安置房面积
        BigDecimal resettlementArea = thirdData.getResettlementArea();
        //54.5安置房户型面积
        BigDecimal areaOne = thirdData.getAreaOne();
        //56.3 审定结构差 与标准户型差面积
        thirdData.setAreaDifference(resettlementArea.subtract(areaOne));
        BigDecimal areaDifference = thirdData.getAreaDifference();
        //54.17 "审定等面积""单价"
        BigDecimal priceThree = thirdData.getPriceThree();

        //56.4 "审定结构差","结构差单价"
        if (areaDifference.compareTo(BigDecimal.ZERO) == 1){
            thirdData.setUnitPrice(priceThree.multiply(num));
        }
        if(areaDifference.compareTo(BigDecimal.ZERO) == -1){
            thirdData.setUnitPrice(priceThree.multiply(num1));
        }


        //56.4 "审定结构差","结构差单价"
        BigDecimal unitPrice = thirdData.getUnitPrice();
        //56.6 "审定结构差","审定金额"
        thirdData.setAmountOne(areaDifference.multiply(unitPrice));
        BigDecimal amountOne = thirdData.getAmountOne();
        //56.2 "结构差","金额"
        BigDecimal moneyThree = thirdData.getMoneyThree();
        //56.5 审计调整
        thirdData.setAdjustmentThree(amountOne.subtract(moneyThree));



        return thirdData;
    }

    //57.3 审定增房数量 （表格数据不完整 待测）
    public ThirdData SetNumTwo(ThirdData thirdData){
        BigDecimal num1 = new BigDecimal(1.1);
        //57.0 "增房（未审数）","数量"
        BigDecimal quantityFour = thirdData.getQuantityFour();
        //57.3 审定增房数量
        thirdData.setNumTwo(quantityFour);
        BigDecimal numTwo = thirdData.getNumTwo();
        //54.17 "审定等面积""单价"
        BigDecimal priceThree = thirdData.getPriceThree();
        //57.4 "审定增房","单价"
        thirdData.setPriceSix(priceThree.multiply(num1));
        BigDecimal priceSix = thirdData.getPriceSix();
        //57.6 "审定增房","审定金额"
        thirdData.setAmountTwo(priceSix.multiply(numTwo));
        BigDecimal amountTwo = thirdData.getAmountTwo();
        //57.2 "增房（未审数）","金额"
        BigDecimal moneyFour = thirdData.getMoneyFour();
        //57.5 "审定增房","审计调整"
        thirdData.setAdjustmentFour(amountTwo.subtract(moneyFour));

        return thirdData;
    }



    //58.3 "审定层次调节费用","安置房层次" （表格数据不完整 待测）
    public ThirdData SetHousingLevel(ThirdData thirdData){
        BigDecimal num2 = new BigDecimal(5);
        BigDecimal num3 = new BigDecimal(50);
        //54.4 "安置单元"
        int unit = thirdData.getUnit();
        //58.3 "审定层次调节费用","安置房层次"
        thirdData.setHousingLevel(unit/100);
        int housingLevel = thirdData.getHousingLevel();
        BigDecimal num1 = new BigDecimal(housingLevel);
        //54.5 安置面积
        BigDecimal resettlementArea = thirdData.getResettlementArea();
        //54.16 "审定等面积","审定数量"
        BigDecimal approvedQuantity = thirdData.getApprovedQuantity();
        //58.4 "审定层次调节费用","㎡"
        thirdData.setAdjustmentCost(resettlementArea.subtract(approvedQuantity));
        BigDecimal adjustmentCost = thirdData.getAdjustmentCost();

        //58.5 "审定层次调节费用","测算系数"
        thirdData.setCoefficientOne(num1.subtract(num2));
        BigDecimal coefficientOne = thirdData.getCoefficientOne();

        //58.6 "审定层次调节费用","单价"
        thirdData.setPriceSeven(num3);
        BigDecimal priceSeven = thirdData.getPriceSeven();

        //58.8 "审定层次调节费用","审定金额"
        thirdData.setAmountFive(adjustmentCost.multiply(coefficientOne).multiply(priceSeven).setScale(BigDecimal.ROUND_HALF_UP));
        BigDecimal amountFive = thirdData.getAmountFive();
        //58.2 "层次调节费用","金额"
        BigDecimal moneyFive = thirdData.getMoneyFive();
        //58.7 "审定层次调节费用","审计调整"
        thirdData.setAdjustmentFive(amountFive.subtract(moneyFive));
        return thirdData;
    }

    //59.0 "安置金额合计"(未审数) OK
    public ThirdData SetTotalAmount(ThirdData thirdData){

        //54.11 "等面积（未审数)","金额"
        BigDecimal moneyOne = thirdData.getMoneyOne();
        //55.02 "上靠（未审数）","金额"
        BigDecimal money = thirdData.getMoney();
        //56.2 "结构差","金额"
        BigDecimal moneyThree = thirdData.getMoneyThree();
        //57.2 "增房（未审数）","金额"
        BigDecimal moneyFour = thirdData.getMoneyFour();
        //58.2 "层次调节费用","金额"
        BigDecimal moneyFive = thirdData.getMoneyFive();
        //59.0 "安置金额合计"
        thirdData.setTotalAmount(moneyOne.add(money).add(moneyThree).add(moneyFour).add(moneyFive));
        BigDecimal totalAmount = thirdData.getTotalAmount();

        //54.19 "审定等面积","审定金额"
        BigDecimal amount = thirdData.getAmount();
        //55.06 "上靠（审定数）","审定金额"
        BigDecimal moneyTwo = thirdData.getMoneyTwo();
        //56.6 "审定结构差","审定金额"
        BigDecimal amountOne = thirdData.getAmountOne();
        //57.6 "审定增房","审定金额"
        BigDecimal amountTwo = thirdData.getAmountTwo();
        //58.8 "审定层次调节费用","审定金额"
        BigDecimal amountFive = thirdData.getAmountFive();

        //59.2 "安置金额合计","","审定金额"
        thirdData.setAmountSix(amount.add(moneyTwo).add(amountOne).add(amountTwo).add(amountFive));
        BigDecimal amountSix = thirdData.getAmountSix();
        //59.1 "安置金额合计","","审计调整"
        thirdData.setAdjustmentSix(amountSix.subtract(totalAmount));

        return thirdData;
    }



    //60.0 决算金额 应收补差价款（正数为交款，负数为退款）OK
    public ThirdData SetReceivablePrice(ThirdData thirdData){
        //59.0 安置金额合计
        BigDecimal totalAmount = thirdData.getTotalAmount();
        //51.0 原房补偿费合计
        BigDecimal originalHouse = thirdData.getOriginalHouse();
        //60.0 决算金额 应收补差价款
        thirdData.setReceivablePrice(totalAmount.subtract(originalHouse));

        return thirdData;
    }

    //61.6 决算交退款 审定数 OK
    public ThirdData SetAmountEight(ThirdData thirdData){
        //60.2 决算金额 审定金额
        BigDecimal amountSix = thirdData.getAmountSix();
        //51.2 原房补偿费审定数
        BigDecimal compensation = thirdData.getCompensation();
        //61.0 预收现金（协议退款）
        BigDecimal cashReceived = thirdData.getCashReceived();
        //61.2 预收现金（抵扣）
        BigDecimal cashReceivedOne = thirdData.getCashReceivedOne();
        //61.3 总登前交退款
        BigDecimal totalRefund = thirdData.getTotalRefund();
        //61.6 决算交退款
        thirdData.setAmountEight(amountSix.subtract(compensation).subtract(cashReceived).subtract(cashReceivedOne).subtract(totalRefund));

        return thirdData;
    }


}
