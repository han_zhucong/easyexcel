package com.hzc.easyexcel.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Classname FirstDataVo
 * @Description TODO
 * @Date 2021/9/26 20:48
 * @Created by hzc
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FirstDataVo {

    @ApiModelProperty(value = "被征收人（产权人）")
    private String owner;

    @ApiModelProperty(value = "结构")
    private String structure;

}
