package com.hzc.easyexcel.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Classname SecondDataVo
 * @Description TODO
 * @Date 2021/9/26 21:25
 * @Created by hzc
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SecondDataVo {

    @ApiModelProperty(value = "被征收人（产权人）")
    private String owner;

    @ApiModelProperty(value = "结构")
    private String structure;

    @ApiModelProperty(value = "查询开始时间", example = "2019-01-01 10:10:10")
    private String begin;//注意，这里使用的是String类型，前端传过来的数据无需进行类型转换

    @ApiModelProperty(value = "查询结束时间", example = "2019-12-01 10:10:10")
    private String end;

}
