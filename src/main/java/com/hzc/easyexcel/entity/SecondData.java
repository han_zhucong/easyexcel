package com.hzc.easyexcel.entity;
/*
 *@auther:陈旭峰
 *@create time:2021/09/18 20:18
 *@description:
 *
 */

import com.alibaba.excel.annotation.ExcelProperty;
import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.hzc.easyexcel.config.IsNullConvertZero;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SecondData {

    private int id;

    //数据库不用建立这个列
    @TableField(exist = false)
    private List<Structure> structureNew; //结构对象


    @ExcelProperty(value = {"序号","序号","序号"},index = 0)
    private int num;

    @ExcelProperty(value = {"上榜顺序号","上榜顺序号","上榜顺序号"},index = 1)
    private int sid;

    @ExcelProperty(value = {"回迁选房顺序号","回迁选房顺序号","回迁选房顺序号"},index = 2)
    private String backId;

    @ExcelProperty(value = {"档案编号","档案编号","档案编号"},index = 3)
    private String fileId;

    @ExcelProperty(value = {"项目地址","项目地址","项目地址"},index = 4)
    private String projectAdd;

    @ExcelProperty(value = {"被征收人（产权人）","被征收人（产权人）","被征收人（产权人）"},index = 5)
    private String owner;

    @ExcelProperty(value = {"原住址","原住址","原住址"},index = 6)
    private String originAdd;

    @ExcelProperty(value = {"结构","结构","结构"},index = 7)
    private String structure;

    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern="yyyy-MM-dd",timezone="GMT+8")
    @ExcelProperty(value = {"签约时间","签约时间","签约时间"},index = 8)
    private String signTime;

    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern="yyyy-MM-dd",timezone="GMT+8")
    @ExcelProperty(value = {"搬迁时间","搬迁时间","搬迁时间"},index = 9)
    private String moveTime;


    @ExcelProperty(value = {"总征收面积（㎡）","总征收面积（㎡）","总征收面积（㎡）"},index = 10)
    private BigDecimal allArea;

    @ExcelProperty(value = {"产权面积（㎡）","住宅","住宅"},index = 11)
    private BigDecimal property;


//    @ExcelProperty(value = {"结构面积","结构面积","结构面积"},index = 8)
//    private String structuralArea;
//
//    @ExcelProperty(value = {"成新率1","成新率1","成新率1"},index = 9)
//    private String firstRate;
//
//    @ExcelProperty(value = {"成新率2","成新率2","成新率2"},index = 10)
//    private String secondRate;
//
//    // TODO: 2021/9/30
//    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern="yyyy-MM-dd",timezone="GMT+8")
//    @ExcelProperty(value = {"签约时间（第一签约期2013.12.31，第二签约期2014.1.15）","签约时间（第一签约期2013.12.31，第二签约期2014.1.15）","签约时间（第一签约期2013.12.31，第二签约期2014.1.15）"},index = 11)
//    private String signTime;





    @ExcelProperty(value = {"84年前","维权100%面积（㎡）","维权100%面积（㎡）"},index = 12)
    private BigDecimal protectionArea;

    @ExcelProperty(value = {"84年-04年","维权70%面积（㎡）","维权70%面积（㎡）"},index = 13)
    private BigDecimal sevenArea;

    @ExcelProperty(value = {"84年-04年","维权50%面积（㎡）","维权50%面积（㎡）"},index = 14)
    private BigDecimal fiveArea;

    @ExcelProperty(value = {"84年-04年","维权20%面积（㎡）","维权20%面积（㎡）"},index = 15)
    private BigDecimal twoArea;

    @ExcelProperty(value = {"84年-04年","其他","其他"},index = 16)
    private BigDecimal otherArea;

    @ExcelProperty(value = {"04年-06年","重置价面积（㎡）","重置价面积（㎡）"},index = 17)
    private BigDecimal resetArea;

    @ExcelProperty(value = {"2006年后","150元面积（㎡）","砖混"},index = 18)
    private BigDecimal brick;

    @ExcelProperty(value = {"2006年后","150元面积（㎡）","砖木"},index = 19)
    private BigDecimal wood;

    @ExcelProperty(value = {"2006年后","150元面积（㎡）","鱼鳞板(核对)"},index = 20)
    private BigDecimal fish;

    @ExcelProperty(value = {"简易房屋面积（㎡）","简易房屋面积（㎡）","简易房屋面积（㎡）"},index = 21)
    private BigDecimal simpleArea;

    @ExcelProperty(value = {"确权面积","（㎡）","审定数"},index = 22)
    private BigDecimal formerArea;

    @ExcelProperty(value = {"公摊面积（㎡）","公摊面积（㎡）","审定数"},index = 23)
    private BigDecimal sharedArea;

    @ExcelProperty(value = {"总确权面积","(㎡)","审定数"},index = 24)
    private BigDecimal authorizedArea;

    //三列空白


//    @ExcelProperty(value = {"","其中：","货币补偿面积"},index = 28)
//    private String monetaryArea;
//
//    @ExcelProperty(value = {"","其中：","产权置换面积"},index = 29)
//    private BigDecimal replacementArea;
//
//    @ExcelProperty(value = {"安置房户型面积","安置房户型面积","安置房户型面积"},index = 30)
//    private BigDecimal housingSize;

    @ExcelProperty(value = {"旧房补偿费（元）","旧房补偿费（元）","旧房补偿费（元）"},index = 25)
    private BigDecimal oldHouseMoney;

    @ExcelProperty(value = {"旧屋补偿土木构单价","",""},index = 26)
    private BigDecimal soilWoodMoney;

    @ExcelProperty(value = {"旧屋补偿砖木单价2","",""},index = 27)
    private String brickWoodMoney;

    @ExcelProperty(value = {"旧屋补偿木购单价3","",""},index = 28)
    private BigDecimal secondSoilWoodMoney;

    @ExcelProperty(value = {"旧屋补偿砖混单价4","",""},index = 29)
    private String secondBrickWoodMoney;

    @ExcelProperty(value = {"审定成新率1","",""},index = 30)
    private String firstNewRate;

    @ExcelProperty(value = {"审定成新率2","",""},index = 31)
    private String secondNewRate;

    @ExcelProperty(value = {"审计调整","",""},index = 32)
    private BigDecimal firstAdjustment;

    @ExcelProperty(value = {"旧房补偿费（元）","","审定数"},index = 33)
    private BigDecimal secondOldHouseMoney;

    @ExcelProperty(value = {"备注及说明","",""},index = 34)
    private String firstInstructions;

    @ExcelProperty(value = {"装修补偿费（元）","装修补偿费（元）","装修补偿费（元）"},index = 35)
    private BigDecimal compensationNo;

    @ExcelProperty(value = {"审计调整","",""},index = 36)
    private BigDecimal secondAdjustment;

    @ExcelProperty(value = {"装修补偿费","（元）","审定数"},index = 37)
    private BigDecimal compensationYes;

    @ExcelProperty(value = {"备注及说明","",""},index = 38)
    private String secondInstructions;

    @ExcelProperty(value = {"区位补偿费","区位补偿费","区位补偿费"},index = 39)
    private BigDecimal locationCompensationNo;

    @ExcelProperty(value = {"审计单价","",""},index = 40)
    private BigDecimal unitPrice;

    @ExcelProperty(value = {"审计调整","",""},index = 41)
    private BigDecimal thirdAdjustment;

    @ExcelProperty(value = {"区位补偿费","（元）","审定数"},index = 42)
    private BigDecimal locationCompensationYes;

    @ExcelProperty(value = {"备注及说明","","差异小于100不予调整"},index = 43)
    private String thirdInstructions;

    @IsNullConvertZero
    @ExcelProperty(value = {"按期搬迁奖励","按期搬迁奖励","按期搬迁奖励"},index = 44)
    private BigDecimal relocationRewardNo;

    @ExcelProperty(value = {"审计调整","",""},index = 45)
    private BigDecimal fourthAdjustment;

    @IsNullConvertZero
    @ExcelProperty(value = {"按期搬迁奖励","（元）","审定数"},index = 46)
    private BigDecimal relocationRewardYes;

    @ExcelProperty(value = {"备注及说明","",""},index = 47)
    private String fourthInstructions;

    @ExcelProperty(value = {"货币补偿奖励","货币补偿奖励","货币补偿奖励"},index = 48)
    private BigDecimal monetaryCompensationNo;

    @ExcelProperty(value = {"货币补偿面积","",""},index = 49)
    private BigDecimal compensationArea;

    @ExcelProperty(value = {"审计单价","",""},index = 50)//BE空白列
    private BigDecimal firstUnitPrice;

    @ExcelProperty(value = {"审计调整","",""},index = 51)
    private BigDecimal fifthAdjustment;

    @ExcelProperty(value = {"货币补偿奖励","（元）","审定数"},index = 52)
    private BigDecimal monetaryCompensationYes;

    @ExcelProperty(value = {"备注及说明","",""},index = 53)
    private String fifthInstructions;

    @ExcelProperty(value = {"安家补贴（元）","安家补贴（元）","安家补贴（元）"},index = 54)
    private BigDecimal relocationAllowanceNo;

    @ExcelProperty(value = {"审计单价","",""},index = 55)
    private BigDecimal secondUnitPrice;

    @ExcelProperty(value = {"审计调整","",""},index = 56)
    private BigDecimal sixthAdjustment;

    @ExcelProperty(value = {"安家补贴","（元）","审定数"},index = 57)
    private BigDecimal relocationAllowanceYes;

    @ExcelProperty(value = {"备注及说明","",""},index = 58)
    private String sixthInstructions;

    @ExcelProperty(value = {"2004年至2006年搬迁奖励（300）","2004年至2006年搬迁奖励（300）","2004年至2006年搬迁奖励（300）"},index = 59)
    private BigDecimal reviewedNo;

    @ExcelProperty(value = {"审计单价","",""},index = 60)
    private BigDecimal thirdUnitPrice;

    @ExcelProperty(value = {"审计调整","",""},index = 61)
    private BigDecimal seventhAdjustment;

    @ExcelProperty(value = {"2004年至2006年搬迁奖励（300）","（元）","审定数"},index = 62)
    private BigDecimal reviewedYes;

    @ExcelProperty(value = {"备注及说明","",""},index = 63)
    private String seventhInstructions;

    @ExcelProperty(value = {"2006年后补贴（150）","",""},index = 64)
    private BigDecimal unauditedSubsidiesNo;

    @ExcelProperty(value = {"审计单价","",""},index = 65)
    private BigDecimal fourthUnitPrice;

    @ExcelProperty(value = {"审计调整","",""},index = 66)
    private BigDecimal audit;

    @ExcelProperty(value = {"2006年后补贴（150）","（元）","审定数"},index = 67)
    private BigDecimal authorizedNum;

    @ExcelProperty(value = {"备注及说明","",""},index = 68)
    private String note;

    @ExcelProperty(value = {"简易","简易","简易"},index = 69)
    private BigDecimal simpleSub;

    @ExcelProperty(value = {"审计单价","",""},index = 70)
    private BigDecimal unitPriceOne;

    @ExcelProperty(value = {"审计调整","",""},index = 71)
    private BigDecimal adjust;

    @ExcelProperty(value = {"简易","（元）","审定数"},index = 72)
    private BigDecimal simpleSubTwo;

    @ExcelProperty(value = {"备注及说明","",""},index = 73)
    private String noteTwo;

    @ExcelProperty(value = {"租房补贴","租房补贴","租房补贴"},index = 74)
    private BigDecimal rentSubOne;

    @ExcelProperty(value = {"审计调整","",""},index = 75)
    private BigDecimal approvedNum;

    @ExcelProperty(value = {"租房补贴","（元）","审定数"},index = 76)
    private BigDecimal rentSubTwo;

    @ExcelProperty(value = {"备注及说明","",""},index = 77)
    private String noteThree;

    @ExcelProperty(value = {"节假日租房补贴","节假日租房补贴","节假日租房补贴"},index = 78)
    private BigDecimal holidayRentPayOne;

    @ExcelProperty(value = {"审计调整","",""},index = 79)
    private BigDecimal adjustNum;

    @ExcelProperty(value = {"节假日租房补贴","（元）","审定数"},index = 80)
    private BigDecimal holidayRentPayTwo;

    @ExcelProperty(value = {"备注及说明","",""},index = 81)
    private String noteFour;

    @ExcelProperty(value = {"搬迁奖励","搬迁奖励","搬迁奖励"},index = 82)
    private BigDecimal moveReward;

    @ExcelProperty(value = {"审计调整","",""},index = 83)
    private String adjustNumTwo;

    @ExcelProperty(value = {"搬迁奖励","（元）","审定数"},index = 84)
    private BigDecimal moveRewardTwo;

    @ExcelProperty(value = {"备注及说明","",""},index = 85)
    private String noteFive;

    @ExcelProperty(value = {"节假日搬迁奖励","节假日搬迁奖励","节假日搬迁奖励"},index = 86)
    private BigDecimal holidayMovePay;

    @ExcelProperty(value = {"审计调整","",""},index = 87)
    private BigDecimal moveRewardThree;

    @ExcelProperty(value = {"节假日搬迁奖励","",""},index = 88)
    private BigDecimal holidayMovePayTwo;

    @ExcelProperty(value = {"备注及说明","",""},index = 89)
    private String noteSix;

    @ExcelProperty(value = {"安置房内部隔墙一次性补偿及卫生洁具补偿","安置房内部隔墙一次性补偿及卫生洁具补偿","安置房内部隔墙一次性补偿及卫生洁具补偿"},index = 90)
    private BigDecimal otherPay;

    @ExcelProperty(value = {"安置房面积","",""},index = 91)
    private BigDecimal area;

    @ExcelProperty(value = {"审计单价","",""},index = 92)
    private BigDecimal onePay;

    @ExcelProperty(value = {"审计调整","",""},index = 93)
    private BigDecimal adjustTwo;

    @ExcelProperty(value = {"安置房内部隔墙一次性补偿及卫生洁具补偿","",""},index = 94)
    private BigDecimal otherPayTwo;

    @ExcelProperty(value = {"备注及说明","",""},index = 95)
    private String noteSeven;

    @ExcelProperty(value = {"搬迁补助费（元）","","10*1"},index = 96)
    private BigDecimal tenPay;

    @ExcelProperty(value = {"搬迁补助费（元）","","15*1"},index = 97)
    private BigDecimal fifteenPay;

    @ExcelProperty(value = {"搬迁补助费（元）","","15*2"},index = 98)
    private BigDecimal fifteenTwoPay;

    @ExcelProperty(value = {"货币安置面积？？","","审定数"},index = 99)//100
    private BigDecimal settlementArea;

    @ExcelProperty(value = {"房屋置换确权面积=18-37.1","","审定数"},index = 100)
    private BigDecimal replaceArea;

    @ExcelProperty(value = {"单价","",""},index = 101)
    private BigDecimal unitPriceTwo;

    @ExcelProperty(value = {"审计调整","","审定数"},index = 102)
    private BigDecimal adjustThree;

    @ExcelProperty(value = {"搬迁补助费（元）","","审定数"},index = 103)
    private BigDecimal allowance;

    @ExcelProperty(value = {"备注及说明","",""},index = 104)
    private String noteEight;

    @ExcelProperty(value = {"临时安置补助费（元）","","8元/月       （12个月）"},index = 105)
    private BigDecimal subsidyOne;

    @ExcelProperty(value = {"临时安置补助费（元）","","8元/月       （36个月）"},index = 106)
    private BigDecimal subsidyTwo;

    @ExcelProperty(value = {"临时安置补助费（元）","","8元/月       （另3个月）"},index = 107)
    private BigDecimal subsidyThree;

    @ExcelProperty(value = {"原房面积","","审定数"},index = 108)
    private BigDecimal originalArea;

    @ExcelProperty(value = {"期房安置面积1","","审定数"},index = 109)
    private BigDecimal placementArea;
    @ExcelProperty(value = {"期房安置面积2","","审定数"},index = 110)
    private BigDecimal placementAreaTwo;
    @ExcelProperty(value = {"期房安置月份","",""},index = 111)
    private BigDecimal resettlemenMonth;

    @ExcelProperty(value = {"审定单价","",""},index = 112)
    private BigDecimal unitPriceThree;

    @ExcelProperty(value = {"审计调整","","审定数"},index = 113)
    private BigDecimal adjustFour;

    @ExcelProperty(value = {"临时安置补助费（元）","","审定数"},index = 114)
    private BigDecimal resettlementMonth;


    @ExcelProperty(value = {"备注及说明","",""},index = 115)
    private String noteNine;


    @ExcelProperty(value = {"店面经营补助","店面经营补助","店面经营补助"},index = 116)
    private BigDecimal subsidy;


    @ExcelProperty(value = {"店面面积","","审定数"},index = 117)
    private BigDecimal storesArea;


    @ExcelProperty(value = {"单价","","审定数"},index = 118)
    private BigDecimal unitPriceFour;

    @ExcelProperty(value = {"店面经营补助","","审定调整"},index = 119)
    private BigDecimal adjustFive;
    @ExcelProperty(value = {"店面经营补助","","审定数"},index = 120)
    private BigDecimal operationSubsidy;


    @ExcelProperty(value = {"备注及说明","",""},index = 121)
    private String noteTen;


    @ExcelProperty(value = {"奖励（06年后）","砖混（150元/㎡）","砖混（150元/㎡）"},index = 122)
    private BigDecimal rewardOne;


    @ExcelProperty(value = {"奖励（06年后）","其他结构（130元/㎡）","其他结构（130元/㎡）"},index = 123)
    private BigDecimal rewardTwo;


    @ExcelProperty(value = {"单价","","审定数"},index = 124)
    private BigDecimal unitPriceFive;


    @ExcelProperty(value = {"审计调整","","审定数"},index = 125)
    private BigDecimal adjustTwoFive;


    @ExcelProperty(value = {"奖励（06年后）","","审定数"},index = 126)
    private BigDecimal rewardThree;

    @ExcelProperty(value = {"备注及说明","",""},index = 127)
    private String noteEleven;

    @ExcelProperty(value = {"租地搬迁奖励","租地搬迁奖励","租地搬迁奖励"},index = 128)
    private BigDecimal relocationRewardOne;

    @ExcelProperty(value = {"租地搬迁奖励","","审定数"},index = 129)
    private BigDecimal relocationReward;

    @ExcelProperty(value = {"备注及说明","",""},index = 130)
    private String noteTwelve;


    @ExcelProperty(value = {"神位搬迁补助","神位搬迁补助","神位搬迁补助"},index = 131)
    private BigDecimal relocationSubsidy;

    @ExcelProperty(value = {"神位搬迁补助","","审定数"},index = 132)
    private BigDecimal relocationSubsidyTwo;

    @ExcelProperty(value = {"备注及说明","",""},index = 133)
    private String noteThirteen;

    @ExcelProperty(value = {"山体护坡","山体护坡","山体护坡"},index = 134)
    private BigDecimal slopeProtection;

    @ExcelProperty(value = {"山体护坡","","审定数"},index = 135)
    private BigDecimal slopeProtectionTwo;


    @ExcelProperty(value = {"备注及说明","",""},index = 136)
    private String noteFourteen;

    @ExcelProperty(value = {"会议纪要？","会议纪要？","会议纪要？"},index = 137)
    private BigDecimal meetingMinutes;

    @ExcelProperty(value = {"会议纪要","","审定数"},index = 138)
    private BigDecimal meetingMinutesTwo;

    @ExcelProperty(value = {"备注及说明","",""},index = 139)
    private String noteFifteen;

    @ExcelProperty(value = {"补偿费","补偿费","补偿费"},index = 140)
    private BigDecimal compensation;

    @ExcelProperty(value = {"补偿费","","审定数"},index = 141)
    private BigDecimal compensationTwo;


    @ExcelProperty(value = {"备注及说明","",""},index = 142)
    private String noteSixteen;


    @ExcelProperty(value = {"决算增加","补过渡费金额","补过渡费金额"},index = 143)
    private BigDecimal accounts;

    @JsonFormat(shape=JsonFormat.Shape.STRING,pattern="yyyy-MM-dd",timezone="GMT+8")
    @ExcelProperty(value = {"回迁时间","",""},index = 144)
    private String fetchTime;

    @ExcelProperty(value = {"面积","","审定数"},index = 145)
    private BigDecimal areaOne;

    @ExcelProperty(value = {"单价1","","审定数"},index = 146)
    private BigDecimal priceOne;
    @ExcelProperty(value = {"单价2","","审定数"},index = 147)
    private BigDecimal priceTwo;
    @ExcelProperty(value = {"月份1","","审定数"},index = 148)
    private int monthOne;
    @ExcelProperty(value = {"月份2","","审定数"},index = 149)
    private int monthTwo;

//    @ExcelProperty(value = {"单价","",""},index = 148)
//    private BigDecimal unitPriceSix;
//
//
//    @ExcelProperty(value = {"月份","","审定数"},index = 149)
//    private BigDecimal month;


    @ExcelProperty(value = {"审计调整","","审定数"},index = 150)
    private BigDecimal adjustSix;


    @ExcelProperty(value = {"决算增加补过渡费金额","","审定数"},index = 151)
    private BigDecimal accountsYes;


    @ExcelProperty(value = {"备注及说明","","审定数"},index = 152)
    private String noteSeventeen;


    @ExcelProperty(value = {"另发逾期过渡费","另发逾期过渡费","另发逾期过渡费"},index = 153)
    private BigDecimal overdue;


    @ExcelProperty(value = {"审计调整","",""},index = 154)
    private BigDecimal unitPriceSeven;


//    @ExcelProperty(value = {"月份","","审定数"},index = 155)
//    private BigDecimal monthOne;
//
//
//    @ExcelProperty(value = {"审计调整","",""},index = 156)
//    private BigDecimal adjustSeven;


    @ExcelProperty(value = {"补过渡费金额","","审定数"},index = 155)
    private BigDecimal overdueOne;


    @ExcelProperty(value = {"备注及说明","",""},index = 156)
    private String noteEighteen;

    @ExcelProperty(value = {"原房补偿补助费合计","原房补偿补助费合计","原房补偿补助费合计"},index = 157)
    private BigDecimal allPay;

    @ExcelProperty(value = {"原房补偿补助费","审计调整","审计调整"},index = 158)
    private BigDecimal auditAdjust;

    @ExcelProperty(value = {"原房补偿补助费","审定数","审定数"},index = 159)
    private BigDecimal originalHouse;

    @ExcelProperty(value = {"原房补偿补助费","备注及说明","备注及说明"},index = 160)
    private String noteNineteen;

    @ExcelProperty(value = {"搬迁时间","搬迁时间","搬迁时间"},index = 161)
    private String moveTimeOne;

    @ExcelProperty(value = {"","索引",""},index = 162)
    private String aindex;

    @ExcelProperty(value = {"安置地点","安置地点","安置地点"},index = 163)
    private String site;



    //判断签约期的属性 第一签约期为0.2   第二签约期为0.1
    private BigDecimal signTimeBySign;

    //判断房屋结构
    //1、钢筋砼一 2、钢筋砼二 3、砖混一等 4、砖混二等  5、砖木一等 6、砖木二等  7、木结构一
    //8、木结构二  9、木结构三  10、土木结构  11、简易结构
    private int judgeStructure;


}
