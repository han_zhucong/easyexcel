package com.hzc.easyexcel.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.hzc.easyexcel.config.IsNullConvertZero;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/*
 *@auther:陈旭峰
 *@create time:2021/09/30 11:21
 *@description:
 *
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ThirdData {



    private int id;

    @ExcelProperty(value = {"序号","序号","序号"},index = 0)
    private int num;

    @ExcelProperty(value = {"档案编号","档案编号","档案编号"},index = 1)
    private String fileId;

    @ExcelProperty(value = {"被征收人（产权人）","被征收人（产权人）","被征收人（产权人）"},index = 2)
    private String owner;

    @ExcelProperty(value = {"原房补偿费合计","原房补偿费合计","原房补偿费合计"},index = 3)
    private BigDecimal originalHouse;

    @ExcelProperty(value = {"重分类","重分类","重分类"},index = 4)
    private BigDecimal reclassification;

    @ExcelProperty(value = {"审计调整","审计调整","审计调整"},index = 5)
    private BigDecimal adjustment;

    @ExcelProperty(value = {"原房补偿费审定数","原房补偿费审定数","原房补偿费审定数"},index = 6)
    private BigDecimal compensation;

    @ExcelProperty(value = {"备注及说明","备注及说明","备注及说明"},index = 7)
    private String description;

    @ExcelProperty(value = {"户型","户型","45"},index = 8)
    private int houseTypeOne;

    @ExcelProperty(value = {"户型","户型","60"},index = 9)
    private int houseTypeTwo;

    @ExcelProperty(value = {"户型","户型","75"},index = 10)
    private int houseTypeThree;

    @ExcelProperty(value = {"户型","户型","90"},index = 11)
    private int houseTypeFour;

    @ExcelProperty(value = {"户型","户型","105"},index = 12)
    private int houseTypeFive;

    @ExcelProperty(value = {"户型","户型","120"},index = 13)
    private int houseTypeSix;

    @ExcelProperty(value = {"户型","户型","135"},index = 14)
    private int houseTypeSeven;

    @ExcelProperty(value = {"协议安置面积","协议安置面积","协议安置面积"},index = 15)
    private BigDecimal agreedArea;

    @ExcelProperty(value = {"安置地点","安置地点","安置地点"},index = 16)
    private String area;

    @ExcelProperty(value = {"安置楼号","安置楼号","安置楼号"},index = 17)
    private int buildingNum;

    @ExcelProperty(value = {"安置单元","安置单元","安置单元"},index = 18)
    private int unit;

    @ExcelProperty(value = {"安置面积","安置面积","安置面积"},index = 19)
    private BigDecimal resettlementArea;

    @ExcelProperty(value = {"安置房户型面积","安置房户型面积","安置房户型面积"},index = 20)
    private BigDecimal areaOne;

    @ExcelProperty(value = {"协议面积（一次结算）","协议面积（一次结算）","协议面积（一次结算）"},index = 21)
    private BigDecimal agreedAreaOne;

    @ExcelProperty(value = {"总登面积（二次决算）","总登面积（二次决算）","总登面积（二次决算）"},index = 22)
    private BigDecimal agreedAreaTwo;

    @ExcelProperty(value = {"总登面积与标准户型差","总登面积与标准户型差","总登面积与标准户型差"},index = 23)
    private BigDecimal difference;

    @ExcelProperty(value = {"等面积（未审数)","等面积（未审数)","数量"},index = 24)
    private BigDecimal quantityOne;

    @ExcelProperty(value = {"等面积（未审数)","等面积（未审数)","单价"},index = 25)
    private BigDecimal priceOne;

    @ExcelProperty(value = {"等面积（未审数)","等面积（未审数)","金额"},index = 26)
    private BigDecimal moneyOne;


    @ExcelProperty(value = {"等面积测算","确权面积","㎡"},index = 27)
    private BigDecimal confirmedArea;

    @ExcelProperty(value = {"等面积测算","扣减货币补偿面积","㎡"},index = 28)
    private BigDecimal compensationArea;

    @ExcelProperty(value = {"等面积测算","测算置换等面积","㎡"},index = 29)
    private BigDecimal replacementArea;

    @ExcelProperty(value = {"等面积测算","与等面积(未审数）差异","㎡"},index = 30)
    private BigDecimal unauditedNumber;

    @ExcelProperty(value = {"审定等面积","审定等面积","审定数量"},index = 31)
    private BigDecimal approvedQuantity;

    @ExcelProperty(value = {"审定等面积","审定等面积","单价"},index = 32)
    private BigDecimal priceThree;

    @ExcelProperty(value = {"审定等面积","审定等面积","审计调整"},index = 33)
    private BigDecimal adjustmentOne;

    @ExcelProperty(value = {"审定等面积","审定等面积","审定金额"},index = 34)
    private BigDecimal amount;

    @ExcelProperty(value = {"审定等面积","审定等面积","备注及说明"},index = 35)
    private String descriptionOne;

    @ExcelProperty(value = {"上靠（未审数）","上靠（未审数）","数量"},index = 36)
    private BigDecimal numOne;
    @ExcelProperty(value = {"上靠（未审数）","上靠（未审数）","单价"},index = 37)
    private BigDecimal price;
    @ExcelProperty(value = {"上靠（未审数）","上靠（未审数）","金额"},index = 38)
    private BigDecimal money;


    @ExcelProperty(value = {"上靠（审定数）","上靠（审定数）","审定上靠面积"},index = 39)
    private BigDecimal quantityTwo;

    @ExcelProperty(value = {"上靠（审定数）","上靠（审定数）","单价"},index = 40)
    private BigDecimal priceTwo;

    @ExcelProperty(value = {"上靠（审定数）","上靠（审定数）","审计调整"},index = 41)
    private BigDecimal adjustmentTwo;

    @ExcelProperty(value = {"上靠（审定数）","上靠（审定数）","审定金额"},index = 42)
    private BigDecimal moneyTwo;

    @ExcelProperty(value = {"上靠（审定数）","上靠（审定数）","备注及说明"},index = 43)
    private String descriptionTwo;

    @ExcelProperty(value = {"结构差","结构差","数量"},index = 44)
    private BigDecimal quantityThree;

    @ExcelProperty(value = {"结构差","结构差","单价"},index = 45)
    private BigDecimal priceFour;

    @ExcelProperty(value = {"结构差","结构差","金额"},index = 46)
    private BigDecimal moneyThree;

    @ExcelProperty(value = {"审定结构差","审定结构差","与标准户型差面积"},index = 47)
    private BigDecimal areaDifference;

    @ExcelProperty(value = {"审定结构差","审定结构差","结构差单价"},index = 48)
    private BigDecimal unitPrice;

    @ExcelProperty(value = {"审定结构差","审定结构差","审计调整"},index = 49)
    private BigDecimal adjustmentThree;

    @ExcelProperty(value = {"审定结构差","审定结构差","审定金额"},index = 50)
    private BigDecimal amountOne;

    @ExcelProperty(value = {"审定结构差","审定结构差","备注及说明"},index = 51)
    private String descriptionThree;

    @ExcelProperty(value = {"增房（未审数）","增房（未审数）","数量"},index = 52)
    private BigDecimal quantityFour;

    @ExcelProperty(value = {"增房（未审数）","增房（未审数）","单价"},index = 53)
    private BigDecimal priceFive;

    @ExcelProperty(value = {"增房（未审数）","增房（未审数）","金额"},index = 54)
    private BigDecimal moneyFour;

    @ExcelProperty(value = {"审定增房","审定增房","数量"},index = 55)
    private BigDecimal numTwo;

    @ExcelProperty(value = {"审定增房","审定增房","单价"},index = 56)
    private BigDecimal priceSix;

    @ExcelProperty(value = {"审定增房","审定增房","审计调整"},index = 57)
    private BigDecimal adjustmentFour;

    @ExcelProperty(value = {"审定增房","审定增房","审定金额"},index = 58)
    private BigDecimal amountTwo;

    @ExcelProperty(value = {"审定增房","审定增房","备注及说明"},index = 59)
    private String descriptionFour;

    @ExcelProperty(value = {"层次调节费用","层次调节费用","㎡"},index = 60)
    private BigDecimal areaTwo;

    @ExcelProperty(value = {"层次调节费用","层次调节费用","系数"},index = 61)
    private BigDecimal coefficient;

    @ExcelProperty(value = {"层次调节费用","层次调节费用","金额"},index = 62)
    private BigDecimal moneyFive;

    @ExcelProperty(value = {"审定层次调节费用","审定层次调节费用","安置房层次"},index = 63)
    private int housingLevel;

    @ExcelProperty(value = {"审定层次调节费用","审定层次调节费用","㎡"},index = 64)
    private BigDecimal adjustmentCost;

    @ExcelProperty(value = {"审定层次调节费用","审定层次调节费用","测算系数"},index = 65)
    private BigDecimal coefficientOne;

    @ExcelProperty(value = {"审定层次调节费用","审定层次调节费用","单价"},index = 66)
    private BigDecimal priceSeven;

    @ExcelProperty(value = {"审定层次调节费用","审定层次调节费用","审计调整"},index = 67)
    private BigDecimal adjustmentFive;

    @ExcelProperty(value = {"审定层次调节费用","审定层次调节费用","审定金额"},index = 68)
    private BigDecimal amountFive;

    @ExcelProperty(value = {"审定层次调节费用","审定层次调节费用","备注及说明"},index = 69)
    private String descriptionFive;

    @ExcelProperty(value = {"安置金额合计","安置金额合计","安置金额合计"},index = 70)
    private BigDecimal totalAmount;

    @ExcelProperty(value = {"安置金额合计","","审计调整"},index = 71)
    private BigDecimal adjustmentSix;

    @ExcelProperty(value = {"安置金额合计","","审定金额"},index = 72)
    private BigDecimal amountSix;

    @ExcelProperty(value = {"安置金额合计","","备注及说明"},index = 73)
    private String descriptionSix;

    @ExcelProperty(value = {"决算金额","应收补差价款（正数为交款，负数为退款）","应收补差价款（正数为交款，负数为退款）"},index = 74)
    private BigDecimal receivablePrice;

    @ExcelProperty(value = {"决算金额","","审计调整"},index = 75)
    private BigDecimal adjustmentSeven;

    @ExcelProperty(value = {"决算金额","","审定金额"},index = 76)
    private BigDecimal amountSeven;

    @ExcelProperty(value = {"决算金额","","备注及说明"},index = 77)
    private String descriptionSeven;

    @ExcelProperty(value = {"其中","预收现金（协议退款）","预收现金（协议退款）"},index = 78)
    private BigDecimal cashReceived;

    @ExcelProperty(value = {"其中","另发逾期过渡费","另发逾期过渡费"},index = 79)
    private BigDecimal overdue;
    @IsNullConvertZero
    @ExcelProperty(value = {"其中","预收现金（抵扣）","预收现金（抵扣）"},index = 80)
    private BigDecimal cashReceivedOne;

    @IsNullConvertZero
    @ExcelProperty(value = {"其中","总登前交退款","总登前交退款"},index = 81)
    private BigDecimal totalRefund;

    @ExcelProperty(value = {"其中","决算交退款","送审数"},index = 82)
    private BigDecimal refund;

    @ExcelProperty(value = {"","决算交退款","审计调整"},index = 83)
    private BigDecimal adjustmentEight;

    @ExcelProperty(value = {"","决算交退款","审定金额"},index = 84)
    private BigDecimal amountEight;

    @ExcelProperty(value = {"","决算交退款","备注及说明"},index = 85)
    private String descriptionEight;

    @ExcelProperty(value = {"备注","备注","备注"},index = 86)
    private String remarks;

    @ExcelProperty(value = {"","索引",""},index = 87)
    private String aindex;



}
