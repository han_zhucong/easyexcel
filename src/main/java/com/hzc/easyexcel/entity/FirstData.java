package com.hzc.easyexcel.entity;

import com.alibaba.excel.annotation.ExcelProperty;
import com.hzc.easyexcel.config.IsNullConvertZero;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @Classname first
 * @Description TODO
 * @Date 2021/9/13 16:30
 * @Created by hzc
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FirstData {

    private int id;

    @ExcelProperty(value = {"序号","序号","序号"},index = 0)
    private int num;

    @ExcelProperty(value = {"上榜顺序号","上榜顺序号","上榜顺序号"},index = 1)
    private int sid;

    @ExcelProperty(value = {"回迁选房顺序号","回迁选房顺序号","回迁选房顺序号"},index = 2)
    private String backId;

    @ExcelProperty(value = {"档案编号","档案编号","档案编号"},index = 3)
    private String fileId;

    @ExcelProperty(value = {"项目地址","项目地址","项目地址"},index = 4)
    private String projectAdd;

    @ExcelProperty(value = {"被征收人（产权人）","被征收人（产权人）","被征收人（产权人）"},index = 5)
    private String owner;

    @ExcelProperty(value = {"原住址","原住址","原住址"},index = 6)
    private String originAdd;

    @ExcelProperty(value = {"结构","结构","结构"},index = 7)
    private String structure;

    @ExcelProperty(value = {"总征收面积（㎡）","总征收面积（㎡）","总征收面积（㎡）"},index = 8)
    private BigDecimal allArea;

    @ExcelProperty(value = {"产权面积（㎡）","住宅","住宅"},index = 9)
    private BigDecimal property;

    @IsNullConvertZero
    @ExcelProperty(value = {"84年前","维权100%面积（㎡）","维权100%面积（㎡）"},index = 10)
    private BigDecimal protectionArea;

    @IsNullConvertZero
    @ExcelProperty(value = {"84年-04年","维权70%面积（㎡）","维权70%面积（㎡）"},index = 11)
    private BigDecimal sevenArea;

    @IsNullConvertZero
    @ExcelProperty(value = {"84年-04年","维权50%面积（㎡）","维权50%面积（㎡）"},index = 12)
    private BigDecimal fiveArea;

    @IsNullConvertZero
    @ExcelProperty(value = {"84年-04年","维权20%面积（㎡）","维权20%面积（㎡）"},index = 13)
    private BigDecimal twoArea;

    @IsNullConvertZero
    @ExcelProperty(value = {"84年-04年","其他","其他"},index = 14)
    private BigDecimal otherArea;

    @IsNullConvertZero
    @ExcelProperty(value = {"04年-06年","重置价面积（㎡）","重置价面积（㎡）"},index = 15)
    private BigDecimal resetArea;

    @IsNullConvertZero
    @ExcelProperty(value = {"2006年后","150元面积（㎡）","砖混"},index = 16)
    private BigDecimal brick;

    @IsNullConvertZero
    @ExcelProperty(value = {"2006年后","150元面积（㎡）","砖木"},index = 17)
    private BigDecimal wood;

    @IsNullConvertZero
    @ExcelProperty(value = {"2006年后","150元面积（㎡）","鱼鳞板"},index = 18)
    private BigDecimal fish;

    @ExcelProperty(value = {"简易房屋面积（㎡）","简易房屋面积（㎡）","简易房屋面积（㎡）"},index = 19)
    private BigDecimal simpleArea;

    @ExcelProperty(value = {"旧房补偿费（元）","旧房补偿费（元）","旧房补偿费（元）"},index = 20)
    private BigDecimal oldHouse;

    @ExcelProperty(value = {"装修补偿费（元）","装修补偿费（元）","装修补偿费（元）"},index = 21)
    private BigDecimal fixPay;

    @ExcelProperty(value = {"区位补偿费","区位补偿费","区位补偿费"},index = 22)
    private BigDecimal areaPay;

    @ExcelProperty(value = {"按期搬迁奖励","按期搬迁奖励","按期搬迁奖励"},index = 23)
    private BigDecimal rewardPay;

    @ExcelProperty(value = {"货币补偿奖励","货币补偿奖励","货币补偿奖励"},index = 24)
    private BigDecimal moneyPay;

    @ExcelProperty(value = {"安家补贴（元）","安家补贴（元）","安家补贴（元）"},index = 25)
    private BigDecimal setUp;

    @ExcelProperty(value = {"2004年至2006年搬迁奖励（300）","2004年至2006年搬迁奖励（300）","2004年至2006年搬迁奖励（300）"},index = 26)
    private BigDecimal movePay;

    @ExcelProperty(value = {"2006年后补贴（150）","2006年后补贴（150）","2006年后补贴（150）"},index = 27)
    private BigDecimal subsidyPay;

    @ExcelProperty(value = {"简易","简易","简易"},index = 28)
    private BigDecimal simplePay;

    @ExcelProperty(value = {"租房补贴","租房补贴","租房补贴"},index = 29)
    private BigDecimal rentalPay;

    @ExcelProperty(value = {"节假日租房补贴","节假日租房补贴","节假日租房补贴"},index = 30)
    private BigDecimal holidayRentalPay;

    @ExcelProperty(value = {"搬迁奖励","搬迁奖励","搬迁奖励"},index = 31)
    private BigDecimal removePay;

    @ExcelProperty(value = {"节假日搬迁奖励","节假日搬迁奖励","节假日搬迁奖励"},index = 32)
    private BigDecimal holidayRemovePay;

    @ExcelProperty(value = {"安置房内部隔墙一次性补偿及卫生洁具补偿","安置房内部隔墙一次性补偿及卫生洁具补偿","安置房内部隔墙一次性补偿及卫生洁具补偿"},index = 33)
    private BigDecimal otherPay;

    @ExcelProperty(value = {"搬迁补助费（元）","搬迁补助费（元）","10*1"},index = 34)
    private BigDecimal tenPay;

    @ExcelProperty(value = {"搬迁补助费（元）","搬迁补助费（元）","15*1"},index = 35)
    private BigDecimal fifteenPay;

    @ExcelProperty(value = {"搬迁补助费（元）","搬迁补助费（元）","15*2"},index = 36)
    private BigDecimal fifteenTwoPay;

    @ExcelProperty(value = {"临时安置补助费（元）","临时安置补助费（元）","8元/月       （12个月）"},index = 37)
    private BigDecimal temporaryPayOne;

    @ExcelProperty(value = {"临时安置补助费（元）","临时安置补助费（元）","8元/月       （36个月）"},index = 38)
    private BigDecimal temporaryPayTwo;

    @ExcelProperty(value = {"临时安置补助费（元）","临时安置补助费（元）","8元/月       （另3个月）"},index = 39)
    private BigDecimal temporaryPayThree;

    @ExcelProperty(value = {"店面经营补助","店面经营补助","店面经营补助"},index = 40)
    private BigDecimal shopPay;

    @ExcelProperty(value = {"奖励（06年后）","砖混（150元/㎡）","砖混（150元/㎡）"},index = 41)
    private BigDecimal brickPay;

    @ExcelProperty(value = {"其他结构（130元/㎡）","其他结构（130元/㎡）","其他结构（130元/㎡）"},index = 42)
    private BigDecimal othersPay;

    @ExcelProperty(value = {"租地搬迁奖励","租地搬迁奖励","租地搬迁奖励"},index = 43)
    private BigDecimal rentOut;

    @ExcelProperty(value = {"神位搬迁补助","神位搬迁补助","神位搬迁补助"},index = 44)
    private BigDecimal godPay;

    @ExcelProperty(value = {"山体护坡","山体护坡","山体护坡"},index = 45)
    private BigDecimal slope;

    @ExcelProperty(value = {"会议纪要","会议纪要","会议纪要"},index = 46)
    private BigDecimal meeting;

    @ExcelProperty(value = {"补偿费","补偿费","补偿费"},index = 47)
    private BigDecimal offsetFee;

    @ExcelProperty(value = {"决算增加","补过渡费金额","补过渡费金额"},index = 48)
    private BigDecimal transfer;

    @ExcelProperty(value = {"另发逾期过渡费","另发逾期过渡费","另发逾期过渡费"},index = 49)
    private BigDecimal otherTransfer;

    @ExcelProperty(value = {"原房补偿补助费合计","原房补偿补助费合计","原房补偿补助费合计"},index = 50)
    private BigDecimal totalPay;
}
