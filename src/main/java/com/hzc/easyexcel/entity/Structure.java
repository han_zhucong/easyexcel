package com.hzc.easyexcel.entity;
/*
 *@auther:陈旭峰
 *@create time:2021/10/03 22:47
 *@description: 结构
 *
 */

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Structure {
    //材料名
    private String name;
    //材料单价格
    private BigDecimal price;

    //面积
    private BigDecimal area1;
    private BigDecimal area2;
    private BigDecimal area3;

    //成新率
    private BigDecimal rate1;
    private BigDecimal rate2;
    private BigDecimal rate3;

    public BigDecimal putPrice(String name){

        switch (name){
            case "钢筋砼一":
                return new BigDecimal(1200);
            case "钢筋砼二":
                return new BigDecimal(900);
            case "砖混一等":
                return new BigDecimal(800);
            case "砖混二等":
                return new BigDecimal(600);
            case "砖木一等":
                return new BigDecimal(700);
            case "砖木二等":
                return new BigDecimal(500);
            case "木结构一":
                return new BigDecimal(1000);
            case "木结构二":
                return new BigDecimal(750);
            case "木结构三":
                return new BigDecimal(500);
            case "土木":
                return new BigDecimal(450);
            case "简易":
                return new BigDecimal(300);
            default:
                return null;

        }


    }

}



