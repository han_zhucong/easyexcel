package com.hzc.easyexcel.Listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.alibaba.excel.exception.ExcelDataConvertException;
import com.alibaba.fastjson.JSON;
import com.hzc.easyexcel.config.IsNullConvertZeroUtil;
import com.hzc.easyexcel.entity.SecondData;
import com.hzc.easyexcel.entity.Structure;
import com.hzc.easyexcel.service.dao.SecondService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

/**
 * @Classname ExcelListener22
 * @Description TODO
 * @Date 2021/9/15 20:04
 * @Created by hzc
 */
public class ExcelListener2 extends AnalysisEventListener<SecondData> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExcelListener2.class);
    /**
     * 每隔5条存储数据库，实际使用中可以3000条，然后清理list ，方便内存回收
     */
    private static final int BATCH_COUNT = 5;
    List<SecondData> list = new ArrayList<SecondData>();

    private SecondService secondService;
    /**
     * 如果使用了spring,请使用这个构造方法。每次创建Listener的时候需要把spring管理的类传进来
     *
     * @param secondService
     */
    public ExcelListener2(SecondService secondService) {
        this.secondService = secondService;
    }

    /**
     * 在转换异常 获取其他异常下会调用本接口。抛出异常则停止读取。如果这里不抛出异常则 继续读取下一行。
     *
     * @param exception
     * @param context
     * @throws Exception
     */
    @Override
    public void onException(Exception exception, AnalysisContext context) {
        LOGGER.error("解析失败，但是继续解析下一行:{}", exception.getMessage());
        // 如果是某一个单元格的转换异常 能获取到具体行号
        // 如果要获取头的信息 配合invokeHeadMap使用
        if (exception instanceof ExcelDataConvertException) {
            ExcelDataConvertException excelDataConvertException = (ExcelDataConvertException)exception;
            LOGGER.error("第{}行，第{}列解析异常", excelDataConvertException.getRowIndex(),
                    excelDataConvertException.getColumnIndex());
        }
    }

    /**
     * 这个每一条数据解析都会来调用
     *
     * @param data
     *            one row value. Is is same as {@link AnalysisContext#readRowHolder()}
     * @param context
     */
    @Override
    public void invoke(SecondData data, AnalysisContext context) {
        LOGGER.info("解析到一条数据:{}", JSON.toJSONString(data));
        IsNullConvertZeroUtil.checkIsNull(data);
//        //储存具体结构 和各自单价
        String structure = data.getStructure();
        List<Structure> list2 = new ArrayList<>();
        if(structure!=null){
            String[] structures = structure.split("、");
            for (String name:structures){
                Structure structure1 = new Structure();
                structure1.setPrice(structure1.putPrice(name));
                structure1.setName(name);
                list2.add(structure1);
            }
        }

        data.setStructureNew(list2);

        list.add(data);
        // 达到BATCH_COUNT了，需要去存储一次数据库，防止数据几万条数据在内存，容易OOM
        if (list.size() >= BATCH_COUNT) {
            saveData();
            // 存储完成清理 list
            list.clear();
        }
    }
    /**
     * 所有数据解析完成了 都会来调用
     *
     * @param context
     */
    @Override
    public void doAfterAllAnalysed(AnalysisContext context) {
        // 这里也要保存数据，确保最后遗留的数据也存储到数据库
        saveData();
        LOGGER.info("所有数据解析完成！");
    }
    /**
     * 加上存储数据库
     */
    private void saveData() {
        LOGGER.info("{}条数据，开始存储数据库！", list.size());
        secondService.saveBatch(list);
        LOGGER.info("存储数据库成功！");
    }
}
