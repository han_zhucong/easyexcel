package com.hzc.easyexcel.service.dao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzc.easyexcel.entity.FirstData;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Classname FirstDao
 * @Description TODO
 * @Date 2021/9/14 0:18
 * @Created by hzc
 */
public interface FirstService extends IService<FirstData> {
    //读取Excel向数据库写入数据
    public void saveFirst(MultipartFile file, FirstService firstService);

    //读取数据库导出数据
    public void writeFirst();
}
