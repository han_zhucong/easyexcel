package com.hzc.easyexcel.service.dao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzc.easyexcel.entity.FirstData;

/**
 * @Classname FirstContractAward
 * @Description TODO
 * @Date 2021/9/26 19:59
 * @Created by hzc
 */
public interface FirstContractAward extends IService<FirstData> {
    //
}
