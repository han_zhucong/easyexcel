package com.hzc.easyexcel.service.dao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzc.easyexcel.entity.ThirdData;

/**
 * @Classname ThirdContractAward
 * @Description TODO
 * @Date 2021/10/3 1:22
 * @Created by hzc
 */
public interface ThirdContractAward extends IService<ThirdData> {
}
