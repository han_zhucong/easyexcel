package com.hzc.easyexcel.service.dao;
/*
 *@auther:陈旭峰
 *@create time:2021/09/30 15:20
 *@description:
 *
 */

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzc.easyexcel.entity.ThirdData;
import org.springframework.web.multipart.MultipartFile;

public interface ThirdService extends IService<ThirdData> {

    //读取Excel向数据库写入数据
    public void saveThird(MultipartFile file, ThirdService thirdService);

    //读取数据库导出数据
    public void writeThird();
}
