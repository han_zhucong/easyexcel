package com.hzc.easyexcel.service.dao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzc.easyexcel.entity.SecondData;

/**
 * @Classname ContractAward
 * @Description TODO
 * @Date 2021/9/20 14:18
 * @Created by hzc
 */
public interface SecondContractAward extends IService<SecondData> {

    //根据名字查询数据
    public SecondData check(String owner);

    //判断签约期
    public boolean contractAward();

}
