package com.hzc.easyexcel.service.dao;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hzc.easyexcel.entity.SecondData;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Classname SecondService
 * @Description TODO
 * @Date 2021/9/15 20:11
 * @Created by hzc
 */
public interface SecondService extends IService<SecondData> {
    //读取Excel向数据库写入数据
    public void saveSecond(MultipartFile file, SecondService secondService);

    //读取数据库导出数据
    public void writeSecond();
}
