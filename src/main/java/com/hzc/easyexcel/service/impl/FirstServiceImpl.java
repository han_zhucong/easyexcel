package com.hzc.easyexcel.service.impl;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hzc.easyexcel.Listener.ExcelListener;
import com.hzc.easyexcel.entity.FirstData;
import com.hzc.easyexcel.mapper.FirstDataMapper;
import com.hzc.easyexcel.service.dao.FirstService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/*
 *@auther:陈旭峰
 *@create time:2021/09/13 19:07
 *@description:
 *
 */
@Service
public class FirstServiceImpl extends ServiceImpl<FirstDataMapper, FirstData> implements FirstService {

    @Override
    public void saveFirst(MultipartFile file, FirstService firstService) {
        InputStream in = null;

        try {
            in = file.getInputStream();

            ExcelReader reader = EasyExcel.read(in).build();
            //EasyExcel.read(in,FirstData.class,new ExcelListener(firstService)).sheet().doRead();
            ReadSheet sheet1 = EasyExcel.readSheet(0).head(FirstData.class).registerReadListener(new ExcelListener(firstService)).build();

            reader.read(sheet1);
            reader.finish();
/*            ExcelReader excelReader = EasyExcel.read(in).build();

            // 第一个sheet读取类型
            ReadSheet readSheet1 = EasyExcel.readSheet(0).head(FirstData.class).registerReadListener(new ExcelListener(firstService)).build();
            // 第二个sheet读取类型
            ReadSheet readSheet2 = EasyExcel.readSheet(0).head(SecondData.class).registerReadListener(new ExcelListener2(secondService)).build();

            excelReader.read(readSheet1,readSheet2);*/

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void writeFirst() {
        String filename = "F:\\test.xlsx";
        EasyExcel.write(filename,FirstData.class).sheet("测试").doWrite(getData());
    }

    //创建方法获取数据
    private List<FirstData> getData(){
        List<FirstData> firstData = baseMapper.selectList(null);
        return firstData;
    }
}
