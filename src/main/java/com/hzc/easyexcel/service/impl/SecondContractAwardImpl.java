package com.hzc.easyexcel.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hzc.easyexcel.entity.SecondData;
import com.hzc.easyexcel.mapper.SecondDataMapper;
import com.hzc.easyexcel.service.dao.SecondContractAward;
import org.springframework.stereotype.Service;

/**
 * @Classname ContractAwardImpl
 * @Description TODO
 * @Date 2021/9/20 14:31
 * @Created by hzc
 */
@Service
public class SecondContractAwardImpl extends ServiceImpl<SecondDataMapper, SecondData> implements SecondContractAward {

    @Override
    public SecondData check(String owner) {
        QueryWrapper<SecondData> wrapper = new QueryWrapper<>();
        wrapper.eq("owner",owner);
        SecondData secondData = baseMapper.selectOne(wrapper);
        return secondData;
    }

    @Override
    public boolean contractAward() {
        return false;
    }
}
