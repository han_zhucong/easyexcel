package com.hzc.easyexcel.service.impl;

import com.alibaba.excel.EasyExcel;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hzc.easyexcel.Listener.ExcelListener2;
import com.hzc.easyexcel.entity.SecondData;
import com.hzc.easyexcel.mapper.SecondDataMapper;
import com.hzc.easyexcel.service.dao.SecondService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

/**
 * @Classname SecondServiceImpl
 * @Description TODO
 * @Date 2021/9/15 20:12
 * @Created by hzc
 */
@Service
public class SecondServiceImpl extends ServiceImpl<SecondDataMapper, SecondData> implements SecondService {
    @Override
    public void saveSecond(MultipartFile file, SecondService secondService) {
        InputStream in = null;
        try {
            in = file.getInputStream();
            EasyExcel.read(in,SecondData.class,new ExcelListener2(secondService)).sheet().doRead();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void writeSecond() {
        String filename = "F:\\test.xlsx";
        EasyExcel.write(filename, SecondData.class).sheet("测试").doWrite(getData());
    }

    //创建方法获取数据
    private List<SecondData> getData(){
        List<SecondData> secondData = baseMapper.selectList(null);
        return secondData;
    }
}
