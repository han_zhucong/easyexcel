package com.hzc.easyexcel.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hzc.easyexcel.entity.FirstData;
import com.hzc.easyexcel.mapper.FirstDataMapper;
import com.hzc.easyexcel.service.dao.FirstContractAward;
import org.springframework.stereotype.Service;

/**
 * @Classname FirstContractAwardImpl
 * @Description TODO
 * @Date 2021/9/26 20:00
 * @Created by hzc
 */
@Service
public class FirstContractAwardImpl extends ServiceImpl<FirstDataMapper, FirstData> implements FirstContractAward {
}
