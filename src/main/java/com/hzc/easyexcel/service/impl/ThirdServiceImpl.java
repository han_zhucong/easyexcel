package com.hzc.easyexcel.service.impl;
/*
 *@auther:陈旭峰
 *@create time:2021/09/30 15:21
 *@description:
 *
 */

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.ExcelReader;
import com.alibaba.excel.read.metadata.ReadSheet;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hzc.easyexcel.Listener.ExcelListener3;
import com.hzc.easyexcel.entity.ThirdData;
import com.hzc.easyexcel.mapper.ThirdDataMapper;
import com.hzc.easyexcel.service.dao.ThirdService;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
@Service
public class ThirdServiceImpl extends ServiceImpl<ThirdDataMapper, ThirdData> implements ThirdService {


    @Override
    public void saveThird(MultipartFile file, ThirdService thirdService) {
        InputStream in = null;

        try {
            in = file.getInputStream();

            ExcelReader reader = EasyExcel.read(in).build();
            //EasyExcel.read(in,FirstData.class,new ExcelListener(firstService)).sheet().doRead();
            ReadSheet sheet1 = EasyExcel.readSheet(0).head(ThirdData.class).registerReadListener(new ExcelListener3(thirdService)).build();

            reader.read(sheet1);
            reader.finish();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void writeThird() {
        String filename = "D:\\test.xlsx";
        EasyExcel.write(filename, ThirdData.class).sheet("测试").doWrite(getData());
    }

    //创建方法获取数据
    private List<ThirdData> getData(){
        List<ThirdData> thirdData = baseMapper.selectList(null);
        return thirdData;
    }
}
