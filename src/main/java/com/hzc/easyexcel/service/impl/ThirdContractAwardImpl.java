package com.hzc.easyexcel.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hzc.easyexcel.entity.ThirdData;
import com.hzc.easyexcel.mapper.ThirdDataMapper;
import com.hzc.easyexcel.service.dao.ThirdContractAward;
import org.springframework.stereotype.Service;

/**
 * @Classname ThirdContractAwardImpl
 * @Description TODO
 * @Date 2021/10/3 1:23
 * @Created by hzc
 */
@Service
public class ThirdContractAwardImpl extends ServiceImpl<ThirdDataMapper, ThirdData> implements ThirdContractAward {
}
