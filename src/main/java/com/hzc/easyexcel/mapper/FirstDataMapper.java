package com.hzc.easyexcel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hzc.easyexcel.entity.FirstData;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Classname FirstDataMapper
 * @Description TODO
 * @Date 2021/9/14 12:09
 * @Created by hzc
 */
@Mapper
public interface FirstDataMapper extends BaseMapper<FirstData> {
}
