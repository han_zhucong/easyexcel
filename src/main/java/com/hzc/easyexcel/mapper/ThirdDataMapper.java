package com.hzc.easyexcel.mapper;
/*
 *@auther:陈旭峰
 *@create time:2021/09/30 15:11
 *@description:
 *
 */

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hzc.easyexcel.entity.ThirdData;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ThirdDataMapper extends BaseMapper<ThirdData> {
}
