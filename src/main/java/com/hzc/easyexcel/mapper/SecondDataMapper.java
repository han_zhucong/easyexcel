package com.hzc.easyexcel.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hzc.easyexcel.entity.SecondData;
import org.apache.ibatis.annotations.Mapper;

/**
 * @Classname SecondDataMapper
 * @Description TODO
 * @Date 2021/9/15 20:10
 * @Created by hzc
 */
@Mapper
public interface SecondDataMapper extends BaseMapper<SecondData> {
}
