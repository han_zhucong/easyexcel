package com.hzc.easyexcel.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hzc.easyexcel.config.CountUtils;
import com.hzc.easyexcel.config.R;
import com.hzc.easyexcel.entity.SecondData;
import com.hzc.easyexcel.entity.Structure;
import com.hzc.easyexcel.entity.vo.SecondDataVo;
import com.hzc.easyexcel.service.dao.SecondContractAward;
import com.hzc.easyexcel.service.dao.SecondService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Classname AwardController
 * @Description TODO
 * @Date 2021/9/20 14:56
 * @Created by hzc
 */
@Api(description = "表二页面")
@RequestMapping("/secondCount")
@RestController
@CrossOrigin
public class SecondAwardController {

    @Autowired
    private SecondService secondService;

    @Autowired
    private SecondContractAward secondContractAward;

    CountUtils countUtils = new CountUtils();

    //判断签约期
    @ApiOperation("判断签约期")
    @GetMapping("judgeSignTime")
    public R judgeSignTime(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.judgeSignTime(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("21.8 旧房补偿费（元）审定数")
    @PostMapping("setSecondOldHouseMoney/{id}")
    public R SetSecondOldHouseMoney(@PathVariable ("id") int id ,@RequestBody Structure structure){
        QueryWrapper<SecondData> wrapper = new QueryWrapper<>();
        wrapper.eq("id",id);
        SecondData one = secondService.getOne(wrapper);
        SecondData secondData = countUtils.setSecondOldHouseMoney(one,structure);
        secondService.updateById(secondData);
        return R.ok();
    }

    @ApiOperation("21.7 旧房补偿费的审计调整")
    @GetMapping("setFirstAdjustment")
    public R SetFirstAdjustment(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetFirstAdjustment(secondDataList.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("23.2 区位补偿费的审计调整")
    @GetMapping("setThirdAdjustment")
    public R SetThirdAdjustment(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetThirdAdjustment(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("23.3 区位补偿费的审定数")
    @GetMapping("setLocationCompensationYes")
    public R SetLocationCompensationYes(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetLocationCompensationYes(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    //按期搬迁奖励
    @ApiOperation("24.1 按期搬迁奖励的审计调整")
    @GetMapping("setFourthAdjustment")
    public R SetFourthAdjustment(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetFourthAdjustment(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("24.2按期搬迁奖励的审定数")
    @GetMapping("setRelocationRewardYes")
    public R SetRelocationRewardYes(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetRelocationRewardYes(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("25.3 货币补偿面积的审计调整")
    @GetMapping("setFifthAdjustment")
    public R SetFifthAdjustment(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetFifthAdjustment(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("25.4 货币补偿奖励的审定数")
    @PostMapping("setMonetaryCompensationYes")
    public R SetMonetaryCompensationYes(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetCompensationArea(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("26.3安家补贴的审定数")
    @GetMapping("setRelocationAllowanceYes")
    public R SetRelocationAllowanceYes(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetRelocationAllowanceYes(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("27.3 2004年至2006年搬迁奖励的审定数")
    @GetMapping("setReviewedYes")
    public R SetReviewedYes(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetReviewedYes(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("27.2 2004年至2006年搬迁奖励的审计调整")
    @GetMapping("setSeventhAdjustment")
    public R SetSeventhAdjustment(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetSeventhAdjustment(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("28.2 2006年后补贴的审计调整")
    @GetMapping("setAudit")
    public R SetAudit(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetAudit(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("28.3 2006年后补贴的审定数")
    @GetMapping("setAuthorizedNum")
    public R SetAuthorizedNum(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetAuthorizedNum(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("29.2 简易补贴的审计调整")
    @GetMapping("setAdjust")
    public R SetAdjust(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetAdjust(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("29.3 简易补贴的审定数")
    @GetMapping("setSimpleSubTwo")
    public R SetSimpleSubTwo(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetSimpleSubTwo(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("50.3 逾期过渡费的审计调整")
    @GetMapping("setAdjustSeven")
    public R SetAdjustSeven(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetAdjustSeven(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("51.0原房补偿补助费（元）合计（未审数）")
    @GetMapping("setAllPay")
    public R SetAllPay(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetAllPay(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("51.2 原房补偿补助费（元）合计审定数")
    @GetMapping("setOriginalHouse")
    public R SetOriginalHouse(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetOriginalHouse(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("安置房面积")
    @GetMapping("setOnePay")
    public R setOnePay(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetOnePay(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }


    @ApiOperation("房屋置换原房确权面积 审计调整")
    @GetMapping("setReplaceArea")
    public R SetReplaceArea(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetReplaceArea(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("40.1 40.5 40.6原房面积 审计调整 审定数")
    @GetMapping("setOriginalArea")
    public R SetOriginalArea(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetOriginalArea(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("41.3 41.4 店面经营补助 审计调整")
    @GetMapping("setOriginal")
    public R SetOriginal(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetOriginal(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }



    @ApiOperation("43.4 奖励（06年后）")
    @GetMapping("setRewardThree")
    public R SetRewardThree(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetRewardThree(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    @ApiOperation("49.4 决算增加补过渡费金额")
    @GetMapping("setAccountsYes")
    public R SetAccountsYes(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.SetAccountsYes(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }



    //分页查询数据
    @GetMapping("page/{current}/{limit}")
    public R pageList(@PathVariable long current,@PathVariable long limit){
        //创建page对象
        Page<SecondData> pageAll = new Page<>(current, limit);
        //实现分页
        secondContractAward.page(pageAll,null);
        //获取所有数据数
        long total = pageAll.getTotal();
        //获取所有记录
        List<SecondData> dataList = pageAll.getRecords();

        HashMap map = new HashMap<>();
        map.put("total",total);
        map.put("rows",dataList);

        return R.ok().data(map);
    }

    //带条件的分页查询
    @PostMapping("pageCondition/{current}/{limit}")
    public R pageListCondition(@PathVariable long current, @PathVariable long limit,
                               @RequestBody(required = false) SecondDataVo secondDataVo){
        //创建page对象
        Page<SecondData> pageAll = new Page<>(current, limit);
        //创建查询条件
        QueryWrapper<SecondData> wrapper = new QueryWrapper<>();

        String owner = secondDataVo.getOwner();
        String structure = secondDataVo.getStructure();
        String begin = secondDataVo.getBegin();
        String end = secondDataVo.getEnd();

        if (!StringUtils.isEmpty(owner)){
            //法1：trim()去掉首尾空格
            //法2：去除查询字段首尾中间所有空格
            //String owner2 = owner.replaceAll(" ", "");
            //法3：可以替换大部分空白字符,不限于空格  \\s* 可以匹配空格、制表符、换页符等空白字符的其中任意一个。
            String owner2 = owner.replaceAll("\\s*", "");
            //模糊查询
            wrapper.like("owner",owner2);
        }
        if (!StringUtils.isEmpty(structure)){
            String structure2 = structure.replaceAll("\\s*", "");
            wrapper.like("structure",structure2);
        }
        if (!StringUtils.isEmpty(begin)){
            wrapper.ge("sign_time",begin);
        }
        if (!StringUtils.isEmpty(end)){
            wrapper.le("sign_time",end);
        }

        //排序
        //wrapper.orderByDesc("owner");

        //分页
        secondContractAward.page(pageAll,wrapper);

        long total = pageAll.getTotal();
        List<SecondData> secondDataList = pageAll.getRecords();

        return R.ok().data("total",total).data("secondDataList",secondDataList);
    }

    //根据名字查询数据
    @GetMapping("getByName/{name}")
    public SecondData getByName(@PathVariable String name){
        SecondData secondData = secondContractAward.check(name);
        System.out.println(secondData);
        return secondData;
    }

    //根据id查询数据
    @GetMapping("getExcelById/{id}")
    public R getExcelById(@PathVariable String id){
        SecondData secondDataById = secondContractAward.getById(id);
        return R.ok().data("secondDataById",secondDataById);
    }

    //根据id更新数据
    @PostMapping("updateExcel/{id}")
    public R updateExcel(@PathVariable String id,@RequestBody(required = false) SecondData secondData){
        secondContractAward.updateById(secondData);
        return R.ok();
    }

    //添加数据
    @PostMapping("addExcel")
    public R addExcel(@RequestBody(required = false) SecondData secondData){
        secondContractAward.save(secondData);
        return R.ok();
    }
}
