package com.hzc.easyexcel.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hzc.easyexcel.config.R;
import com.hzc.easyexcel.config.ThirdCountUtils;
import com.hzc.easyexcel.entity.SecondData;
import com.hzc.easyexcel.entity.ThirdData;
import com.hzc.easyexcel.service.dao.SecondContractAward;
import com.hzc.easyexcel.service.dao.ThirdContractAward;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @Classname ThirdAwardController
 * @Description TODO
 * @Date 2021/10/3 1:11
 * @Created by hzc
 */
@Api(description = "表四页面")
@RequestMapping("/thirdCount")
@RestController
@CrossOrigin
public class ThirdAwardController {

    @Autowired
    private SecondContractAward secondContractAward;

    @Autowired
    private ThirdContractAward thirdContractAward;

    ThirdCountUtils thirdCountUtils = new ThirdCountUtils();

    @ApiOperation("54.12 等面积金额测算")
    @GetMapping("/setMoneyOne")
    public R SetMoneyOne(){
        ArrayList<ThirdData> thirdDataList = new ArrayList<>();
        List<SecondData> secondDataSelectList = secondContractAward.list(null);
        int num = 0;
        for (int i = 0; i < secondDataSelectList.size(); i++) {
            SecondData secondData = secondDataSelectList.get(i);
            QueryWrapper<ThirdData> wrapper = new QueryWrapper<>();
            num = secondData.getNum();
            wrapper.eq("num",num);
            ThirdData one = thirdContractAward.getOne(wrapper);
            ThirdData thirdData = thirdCountUtils.SetConfirmedArea(secondData, one);
            thirdDataList.add(thirdData);
//            if (num != 0){
//                for (int j = 0; j < thirdDataSelectList.size(); j++) {
//                    ThirdData thirdData = thirdDataSelectList.get(j);
//                    num2 = thirdData.getNum();
//                    if (num == num2){
//                        ThirdData thirdData2 = thirdCountUtils.SetMoneyOne(secondData, thirdData);
//                        thirdDataList.add(thirdData2);
//                    }
//                }
//            }
        }
        thirdContractAward.updateBatchById(thirdDataList);
        return R.ok();
    }

    @ApiOperation("54.13 等面积金额测算（扣减货币补偿面积）")
    @GetMapping("setCompensationArea")
    public R SetCompensationArea(){
        ArrayList<ThirdData> thirdDataList = new ArrayList<>();
        List<SecondData> secondDataSelectList = secondContractAward.list(null);
        int num = 0;
        for (int i = 0; i < secondDataSelectList.size(); i++) {
            SecondData secondData = secondDataSelectList.get(i);
            num = secondData.getNum();
            QueryWrapper<ThirdData> wrapper = new QueryWrapper<>();
            wrapper.eq("num",num);
            ThirdData one = thirdContractAward.getOne(wrapper);
            ThirdData thirdData = thirdCountUtils.SetCompensationArea(secondData, one);
            thirdDataList.add(thirdData);
        }
        thirdContractAward.updateBatchById(thirdDataList);
        return R.ok();
    }

    @ApiOperation("54.14 等面积金额（测算测算置换等面积）")
    @GetMapping("setReplacementArea")
    public R SetReplacementArea(){
        ArrayList<ThirdData> thirdDataList = new ArrayList<>();
        List<ThirdData> thirdDataListSelect = thirdContractAward.list(null);
        for (int i = 0; i < thirdDataListSelect.size(); i++) {
            ThirdData thirdData = thirdCountUtils.SetReplacementArea(thirdDataListSelect.get(i));
            thirdDataList.add(thirdData);
        }
        thirdContractAward.updateBatchById(thirdDataList);
        return R.ok();
    }

    @ApiOperation("54.15等面积金额（与等面积差异面积）")
    @GetMapping("setUnauditedNumber")
    public R SetUnauditedNumber(){
        ArrayList<ThirdData> thirdDataList = new ArrayList<>();
        List<ThirdData> thirdDataListSelect = thirdContractAward.list(null);
        for (int i = 0; i < thirdDataListSelect.size(); i++) {
            ThirdData thirdData = thirdCountUtils.SetUnauditedNumber(thirdDataListSelect.get(i));
            thirdDataList.add(thirdData);
        }
        thirdContractAward.updateBatchById(thirdDataList);
        return R.ok();
    }

    @ApiOperation("54.16 审定等面积 审定数量")
    @GetMapping("setApprovedQuantity")
    public R SetApprovedQuantity(){
        ArrayList<ThirdData> thirdDataList = new ArrayList<>();
        List<ThirdData> thirdDataListSelect = thirdContractAward.list(null);
        for (int i = 0; i < thirdDataListSelect.size(); i++) {
            ThirdData thirdData = thirdCountUtils.SetApprovedQuantity(thirdDataListSelect.get(i));
            thirdDataList.add(thirdData);
        }
        thirdContractAward.updateBatchById(thirdDataList);
        return R.ok();
    }

    @ApiOperation("54.17 审定等面积 单价")
    @GetMapping("setPriceThree")
    public R SetPriceThree(){
        ArrayList<ThirdData> thirdDataList = new ArrayList<>();
        List<ThirdData> thirdDataListSelect = thirdContractAward.list(null);
        for (int i = 0; i < thirdDataListSelect.size(); i++) {
            ThirdData thirdData = thirdCountUtils.SetPriceThree(thirdDataListSelect.get(i));
            thirdDataList.add(thirdData);
        }
        thirdContractAward.updateBatchById(thirdDataList);
        return R.ok();
    }

    @ApiOperation("54.18 审定等面积金额 审计调整")
    @GetMapping("setAdjustmentOne")
    public R SetAdjustmentOne(){
        ArrayList<ThirdData> thirdDataList = new ArrayList<>();
        List<ThirdData> thirdDataListSelect = thirdContractAward.list(null);
        for (int i = 0; i < thirdDataListSelect.size(); i++) {
            ThirdData thirdData = thirdCountUtils.SetAdjustmentOne(thirdDataListSelect.get(i));
            thirdDataList.add(thirdData);
        }
        thirdContractAward.updateBatchById(thirdDataList);
        return R.ok();
    }

    @ApiOperation("54.19 审定等面积 审定金额")
    @GetMapping("setAmount")
    public R SetAmount(){
        ArrayList<ThirdData> thirdDataList = new ArrayList<>();
        List<ThirdData> thirdDataListSelect = thirdContractAward.list(null);
        for (int i = 0; i < thirdDataListSelect.size(); i++) {
            ThirdData thirdData = thirdCountUtils.SetAmount(thirdDataListSelect.get(i));
            thirdDataList.add(thirdData);
        }
        thirdContractAward.updateBatchById(thirdDataList);
        return R.ok();
    }

    @ApiOperation("55.03 审定上靠 审定上靠面积")
    @GetMapping("setQuantityTwo")
    public R SetQuantityTwo(){
        ArrayList<ThirdData> thirdDataList = new ArrayList<>();
        List<ThirdData> thirdDataListSelect = thirdContractAward.list(null);
        for (int i = 0; i < thirdDataListSelect.size(); i++) {
            ThirdData thirdData = thirdCountUtils.SetQuantityTwo(thirdDataListSelect.get(i));
            thirdDataList.add(thirdData);
        }
        thirdContractAward.updateBatchById(thirdDataList);
        return R.ok();
    }

    @ApiOperation("55.04 审定上靠 单价")
    @GetMapping("setPriceTwo")
    public R SetPriceTwo(){
        ArrayList<ThirdData> thirdDataList = new ArrayList<>();
        List<ThirdData> thirdDataListSelect = thirdContractAward.list(null);
        for (int i = 0; i < thirdDataListSelect.size(); i++) {
            ThirdData thirdData = thirdCountUtils.SetPriceTwo(thirdDataListSelect.get(i));
            thirdDataList.add(thirdData);
        }
        thirdContractAward.updateBatchById(thirdDataList);
        return R.ok();
    }

    @ApiOperation("55.05 审计调整")
    @GetMapping("setAdjustmentTwo")
    public R SetAdjustmentTwo(){
        ArrayList<ThirdData> thirdDataList = new ArrayList<>();
        List<ThirdData> thirdDataListSelect = thirdContractAward.list(null);
        for (int i = 0; i < thirdDataListSelect.size(); i++) {
            ThirdData thirdData = thirdCountUtils.SetAdjustmentTwo(thirdDataListSelect.get(i));
            thirdDataList.add(thirdData);
        }
        thirdContractAward.updateBatchById(thirdDataList);
        return R.ok();
    }

    @ApiOperation("55.06 审顶上靠 金额")
    @GetMapping("setMoneyTwo")
    public R SetMoneyTwo(){
        ArrayList<ThirdData> thirdDataList = new ArrayList<>();
        List<ThirdData> thirdDataListSelect = thirdContractAward.list(null);
        for (int i = 0; i < thirdDataListSelect.size(); i++) {
            ThirdData thirdData = thirdCountUtils.SetMoneyTwo(thirdDataListSelect.get(i));
            thirdDataList.add(thirdData);
        }
        thirdContractAward.updateBatchById(thirdDataList);
        return R.ok();
    }

    @ApiOperation("56.3 56.4 56.5 56.6 审定结构差 与标准户型差面积")
    @GetMapping("setAreaDifference")
    public R SetAreaDifference(){
        ArrayList<ThirdData> thirdDataList = new ArrayList<>();
        List<ThirdData> thirdDataListSelect = thirdContractAward.list(null);
        for (int i = 0; i < thirdDataListSelect.size(); i++) {
            ThirdData thirdData = thirdCountUtils.SetAreaDifference(thirdDataListSelect.get(i));
            thirdDataList.add(thirdData);
        }
        thirdContractAward.updateBatchById(thirdDataList);
        return R.ok();
    }

    @ApiOperation("57.3 57.4 57.5 57.6 审定增房数量")
    @GetMapping("setNumTwo")
    public R SetNumTwo(){
        ArrayList<ThirdData> thirdDataList = new ArrayList<>();
        List<ThirdData> thirdDataListSelect = thirdContractAward.list(null);
        for (int i = 0; i < thirdDataListSelect.size(); i++) {
            ThirdData thirdData = thirdCountUtils.SetNumTwo(thirdDataListSelect.get(i));
            thirdDataList.add(thirdData);
        }
        thirdContractAward.updateBatchById(thirdDataList);
        return R.ok();
    }

    @ApiOperation("58.3 58.4 58.5 58.7 58.8 审定层次调节费用 安置房层次")
    @GetMapping("setHousingLevel")
    public R SetHousingLevel(){
        ArrayList<ThirdData> thirdDataList = new ArrayList<>();
        List<ThirdData> thirdDataListSelect = thirdContractAward.list(null);
        for (int i = 0; i < thirdDataListSelect.size(); i++) {
            ThirdData thirdData = thirdCountUtils.SetHousingLevel(thirdDataListSelect.get(i));
            thirdDataList.add(thirdData);
        }
        thirdContractAward.updateBatchById(thirdDataList);
        return R.ok();
    }

    @ApiOperation("59.0 59.1 59.2 安置金额合计(未审数)")
    @GetMapping("setTotalAmount")
    public R SetTotalAmount(){
        ArrayList<ThirdData> thirdDataList = new ArrayList<>();
        List<ThirdData> thirdDataListSelect = thirdContractAward.list(null);
        for (int i = 0; i < thirdDataListSelect.size(); i++) {
            ThirdData thirdData = thirdCountUtils.SetTotalAmount(thirdDataListSelect.get(i));
            thirdDataList.add(thirdData);
        }
        thirdContractAward.updateBatchById(thirdDataList);
        return R.ok();
    }




    @ApiOperation("60.0 60.2 决算金额 应收补差价款")
    @GetMapping("setReceivablePrice")
    public R SetReceivablePrice(){
        ArrayList<ThirdData> thirdDataList = new ArrayList<>();
        List<ThirdData> thirdDataListSelect = thirdContractAward.list(null);
        for (int i = 0; i < thirdDataListSelect.size(); i++) {
            ThirdData thirdData = thirdCountUtils.SetReceivablePrice(thirdDataListSelect.get(i));
            thirdDataList.add(thirdData);
        }
        thirdContractAward.updateBatchById(thirdDataList);
        return R.ok();
    }


    @ApiOperation("61.6 决算交退款 审定数")
    @GetMapping("setAmountEight")
    public R SetAmountEight(){
        ArrayList<ThirdData> thirdDataList = new ArrayList<>();
        List<ThirdData> thirdDataListSelect = thirdContractAward.list(null);
        for (int i = 0; i < thirdDataListSelect.size(); i++) {
            ThirdData thirdData = thirdCountUtils.SetAmountEight(thirdDataListSelect.get(i));
            thirdDataList.add(thirdData);
        }
        thirdContractAward.updateBatchById(thirdDataList);
        return R.ok();
    }

}
