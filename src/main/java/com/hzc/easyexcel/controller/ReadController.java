package com.hzc.easyexcel.controller;

import com.hzc.easyexcel.service.dao.FirstService;
import com.hzc.easyexcel.service.dao.SecondService;
import com.hzc.easyexcel.service.dao.ThirdService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @Classname ReadController
 * @Description TODO
 * @Date 2021/9/14 14:21
 * @Created by hzc
 */
@Api(description = "读写数据")
@RequestMapping("/excel")
@RestController
public class ReadController {
    @Autowired
    private FirstService firstService;

    @Autowired
    private SecondService secondService;

    @Autowired
    private ThirdService thirdService;

    //读取Excel插入数据库
    @PostMapping("readData")
    public void ReadData(MultipartFile file){
        firstService.saveFirst(file, firstService);
    }

    //读取数据库导出Excel
    @GetMapping("writeData")
    public void WriteData(){
        firstService.writeFirst();
    }

    //第二个表
    @PostMapping("readData2")
    public void ReadData2(MultipartFile file){
        secondService.saveSecond(file,secondService);
    }

    //第二个表
    @GetMapping("writeData2")
    public void WriteData2(){
        secondService.writeSecond();
    }

    //第三个表
    @PostMapping("readData3")
    public void ReadData3(MultipartFile file){
        thirdService.saveThird(file,thirdService);
    }

    //第三个表
    @GetMapping("writeData3")
    public void WriteData3(){
        thirdService.writeThird();
    }
}
