package com.hzc.easyexcel.controller;

import com.hzc.easyexcel.config.R;
import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.*;

/**
 * @Classname LoginController
 * @Description TODO
 * @Date 2021/9/25 18:03
 * @Created by hzc
 */
@Api(description = "登录")
@RestController
@RequestMapping("/user")
@CrossOrigin
public class LoginController {

    @PostMapping("/login")
    public R login(){
        return R.ok().data("token","admin");
    }

    @GetMapping("/info")
    public R getInfo(){
        return R.ok().data("roles","[admin]").data("name","admin").data("avatar","http://www.baidu.com/link?url=n0vYV8erYkAJo9JaDQJolHZNOWTC5JOqrCvW_SQHfCP4kSR3ha9riPN7Jv8iDJd2cit3cWndLiTn3njefNfg3P5YbLcW3Ip5hzXU31yS1LYbBQU7FlfJWgQ1bwLdFJx3cToqI9KKwJ3aF2DU380NMV3CLLFAcikaTGM-bDkLkgrBPUec2oFoHtsTie3OO8qsPHFRtSZ2pXRbHpuxlh4MaY_FmA9SSwzg5mIQjuNcD0jfT9mb0K-NRdKRMZAS7DgfVCLmRUVcHMgmVM8Nb9neab61AKKDY2o7IOXw2jQ6t__Ob6BP4UdmD5AAvhzViKlKIlHxNWCYFA1Hkfi8420Psj-ZcfwBtofcpUESwfjJo8kovqOmlsbadZxTcg0WGUFK4ZcI1fcGtZak8iTGGT0ihjIDhsCYnkEbsyUSdiF_fN9cGp3YdSgS6M9H0E8YuslDtYJIE3oA_yLt0gFVytF36XAUBTjqwxVB3m86iW9ITgqcpVkmSgyW38lpGPTUzR-2tTA0yQQulsGQXrJu307OiK6g518MRf2g47o8WlgfjkComS-tGw4shV9dzr8mK-iDf-En2IGJmZHf2Mus5YRibPT5FkDyZkIG5SPhGe408jK");
    }
}
