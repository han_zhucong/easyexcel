package com.hzc.easyexcel.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.hzc.easyexcel.config.CountUtils;
import com.hzc.easyexcel.config.R;
import com.hzc.easyexcel.entity.FirstData;
import com.hzc.easyexcel.entity.SecondData;
import com.hzc.easyexcel.entity.vo.FirstDataVo;
import com.hzc.easyexcel.service.dao.FirstContractAward;
import com.hzc.easyexcel.service.dao.SecondContractAward;
import com.hzc.easyexcel.service.dao.SecondService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Classname FirstAwardController
 * @Description TODO
 * @Date 2021/9/26 19:57
 * @Created by hzc
 */
@Api(description = "表一页面")
@RequestMapping("/firstCount")
@RestController
@CrossOrigin
public class FirstAwardController {

    @Autowired
    private SecondService secondService;

    @Autowired
    private FirstContractAward firstContractAward;

    @Autowired
    private SecondContractAward secondContractAward;

    CountUtils countUtils = new CountUtils();

    //计算房屋确权面积并插入数据库
    @GetMapping("getFormerArea")
    public R getAllList(){
        BigDecimal formerArea = null;
        List<FirstData> firstDataList = firstContractAward.list(null);
        //List<SecondData> secondDataList = secondContractAward.list(null);
        for (int i = 0; i < firstDataList.size(); i++) {
            //计算
            formerArea = countUtils.countFormerArea(firstDataList.get(i));
            //比较sid，查询对应对象SecondData,存入数据库
            int num = firstDataList.get(i).getNum();
            //比较表一与表二ID
            QueryWrapper<SecondData> wrapper = new QueryWrapper<>();
            wrapper.eq("num",num);
            SecondData secondData = secondService.getOne(wrapper);
            System.out.println(formerArea);
            secondData.setFormerArea(formerArea);

            secondService.update(secondData,wrapper);
        }
        return R.ok();
    }

    //获取公摊面积  OK
    @GetMapping("getSharedArea")
    public R getSharedArea(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.countSharedArea(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }

    //获取确权面积  OK
    @GetMapping("getAuthorizedArea")
    public R getAuthorizedArea(){
        ArrayList<SecondData> secondDataList = new ArrayList<>();
        List<SecondData> secondDataListSelect = secondContractAward.list(null);
        for (int i = 0; i < secondDataListSelect.size(); i++) {
            SecondData secondData = countUtils.countAuthorizedArea(secondDataListSelect.get(i));
            secondDataList.add(secondData);
        }
        secondService.updateBatchById(secondDataList);
        return R.ok();
    }



    //分页查询数据
    @GetMapping("page/{current}/{limit}")
    public R pageList(@PathVariable long current, @PathVariable long limit){
        //创建page对象
        Page<FirstData> pageAll = new Page<>(current, limit);
        //实现分页
        firstContractAward.page(pageAll,null);
        //获取所有数据数
        long total = pageAll.getTotal();
        //获取所有记录
        List<FirstData> dataList = pageAll.getRecords();

        HashMap map = new HashMap<>();
        map.put("total",total);
        map.put("rows",dataList);

        return R.ok().data(map);
    }

    //带条件的分页查询
    @PostMapping("pageCondition/{current}/{limit}")
    public R pageListCondition(@PathVariable long current, @PathVariable long limit,
                               @RequestBody(required = false) FirstDataVo firstDataVo){
        //创建page对象
        Page<FirstData> pageAll = new Page<>(current, limit);
        //创建查询条件
        QueryWrapper<FirstData> wrapper = new QueryWrapper<>();

        String owner = firstDataVo.getOwner();
        String structure = firstDataVo.getStructure();

        if (!StringUtils.isEmpty(owner)){
            String owner2 = owner.replaceAll("\\s*", "");
            wrapper.like("owner",owner2);
        }
        if (!StringUtils.isEmpty(structure)){
            String structure2 = structure.replaceAll("\\s*", "");
            wrapper.like("structure",structure2);
        }

        //排序
        //wrapper.orderByDesc("owner");

        //分页
        firstContractAward.page(pageAll,wrapper);

        long total = pageAll.getTotal();
        List<FirstData> firstDataList = pageAll.getRecords();

        return R.ok().data("total",total).data("firstDataList",firstDataList);
    }

    //根据id查询数据
    @GetMapping("getExcelById/{id}")
    public R getExcelById(@PathVariable String id){
        FirstData firstDataById = firstContractAward.getById(id);
        return R.ok().data("firstDataById",firstDataById);
    }

    //根据id更新数据
    @PostMapping("updateExcel/{id}")
    public R updateExcel(@PathVariable String id,@RequestBody(required = false) FirstData firstData){
        firstContractAward.updateById(firstData);
        return R.ok();
    }

    //添加数据
    @PostMapping("addExcel")
    public R addExcel(@RequestBody(required = false) FirstData firstData){
        firstContractAward.save(firstData);
        return R.ok();
    }
}
