package com.hzc.easyexcel;

import com.alibaba.fastjson.JSON;
import com.hzc.easyexcel.entity.Structure;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
class EasyexcelApplicationTests {

    @Test
    void contextLoads() {
        List<Structure> list = new ArrayList<>();
        for (int i=0;i<5;i++){
            Structure structure = new Structure();
            structure.setName("ni"+i);
            list.add(structure);
        }

        String s = JSON.toJSONString(list);
        System.out.println(s);
        List<Structure> list1 = JSON.parseArray(s, Structure.class);
        System.out.println(list1);

    }

}
